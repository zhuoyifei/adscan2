 var summary01Data = null;
 var summary02Data = null;
 var summary03Data = null;

 var summary01DataA = summary01DataB = summary01DataC = null;

 function renderPage4(refreshNext) {

     var level = $('#flevel3').combobox('getValue')

     $.get("/summarymobile01a/" + level, {}, function(data, error) {
         summary01DataA = JSON.parse(data);
         //隐藏表格header
         $('td[field=a0]')[0].parentNode.parentNode.parentNode.parentNode.parentNode.style.display = "none";
     });

     $.get("/summarymobile01b/" + level, {}, function(data, error) {
         summary01DataB = JSON.parse(data);
     });

     $.get("/summarymobile01c/" + level, {}, function(data, error) {
         summary01DataC = JSON.parse(data);
     });

     $.get("/summarymobile02/" + level, {}, function(data, error) {
         summary02Data = JSON.parse(data);
         fixsummary02Data();

         //隐藏表格header
         $('td[field=b0]')[0].parentNode.parentNode.parentNode.parentNode.parentNode.style.display = "none";

         $.get("/summarymobile03", {}, function(data, error) {
             summary03Data = JSON.parse(data);
             fixsummary03Data();

             changeStyleMediaSpending();

             inRunning = false;
             startRunProgress();


         });
     });

 }

 function fixsummary02Data() {
     var level = vm.tab4_level
     var arr = getType("rc" + level);
     for (var i = 0; i < arr.length; i++) {
         var t = summary02Data[i + 2];
         for (var j = 0; j < 21; j++) {
             t["b" + (j + 2)] = Math.round(arr[i]["f" + j]); //100;
         }
     }
     var config = vm.cfg;
     summary02Data[0]["b0"] = config.cityText;
 }

 function fixsummary03Data() {
     var arr = getType("total_spend")
     for (var i = 0; i < arr.length; i++) {
         var t = summary03Data[i + 2];
         for (var j = 0; j < 21; j++) {
             t["c" + (j + 2)] = Math.round(arr[i]["f" + j] / 1000);
         }
     }
     var config = vm.cfg;
     summary03Data[0]["c0"] = config.cityText;
 }

 function getType(type) {
     var currentSummary = initData.summary;
     var arr = [];
     for (var i = 0; i < currentSummary.length; i++) {
         var t = currentSummary[i];
         if (t.type == type)
             arr.push(t)
     }
     return arr;
 }

 var styleXY = [];

 function changeStyleAudienceReach() {
     if (summary02Data == null) return;

     var summary01Data = summary01DataC.slice(0);
     var v = 50.00;
     v = parseFloat(v);
     var styleXY = [];

     for (var j = 2; j <= 22; j++) {
         var p = 2;
         var min = 99999999;
         for (var i = 2; i < summary02Data.length; i++) {
             var t = summary02Data[i];

             var c = Math.abs(t["b" + j] * 1 - v);

             if (min >= c) {
                 p = i;
                 min = c;
             }
             console.log(t["b" + j] * 1 + " 相差： " + c + " 最小：" + min + " 行：" + p);
         }
         styleXY.push([p, j]);
     }

     for (var i = styleXY.length - 1; i > -1; i--) {
         var p = styleXY[i];
         var row = summary03Data[p[0]];
         //计算ar
         summary01Data[0]["a" + (i + 2)] = summary02Data[p[0]]["b" + p[1]] + "%";
         //计算grp tv
         summary01Data[1]["a" + (i + 2)] = summary03Data[p[0]]["c1"];
         //计算grp online
         summary01Data[2]["a" + (i + 2)] = summary03Data[1]["c" + p[1]];
         //计算grp total
         summary01Data[3]["a" + (i + 2)] = summary01Data[1]["a" + (i + 2)] * 1 + summary01Data[2]["a" + (i + 2)] * 1;
         //计算ms tv
         summary01Data[4]["a" + (i + 2)] = summary03Data[p[0]]["c2"];
         //计算ms online
         summary01Data[5]["a" + (i + 2)] = summary03Data[2]["c" + p[1]];
         //计算ms total
         summary01Data[6]["a" + (i + 2)] = summary03Data[p[0]]["c" + p[1]];

     }
     return summary01Data
 }



 function changeStyleMediaSpending() {
     if (summary03Data == null) return;
     summary01Data = summary01DataA.slice(0);
     var v = 55.00;
     v = parseFloat(v);
     styleXY = [];
     for (var j = 2; j <= 22; j++) {
         var p = 2;
         var min = 99999999;
         for (var i = 2; i < summary03Data.length; i++) {
             var t = summary03Data[i];

             var c = Math.abs(t["c" + j] * 1 - v);

             if (min >= c) {
                 p = i;
                 min = c;
             }
         }
         styleXY.push([p, j]);
     }

     for (var i = styleXY.length - 1; i > -1; i--) {
         var p = styleXY[i];
         var row = summary03Data[p[0]];
         //计算ms total
         summary01Data[0]["a" + (i + 2)] = summary03Data[p[0]]["c" + p[1]];
         //计算ms tv
         summary01Data[1]["a" + (i + 2)] = summary03Data[p[0]]["c2"];
         //计算ms online
         summary01Data[2]["a" + (i + 2)] = summary03Data[2]["c" + p[1]];
         //计算grp tv
         summary01Data[3]["a" + (i + 2)] = summary03Data[p[0]]["c1"];
         //计算grp online
         summary01Data[4]["a" + (i + 2)] = summary03Data[1]["c" + p[1]];
         //计算grp total
         summary01Data[5]["a" + (i + 2)] = summary01Data[3]["a" + (i + 2)] * 1 + summary01Data[4]["a" + (i + 2)] * 1;
         //计算ar
         summary01Data[6]["a" + (i + 2)] = Math.round(summary02Data[p[0]]["b" + p[1]] * 100) / 10000;

     }
     console.log(summary01Data);

 }
