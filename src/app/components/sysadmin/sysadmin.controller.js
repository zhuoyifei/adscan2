(function() {
    'use strict';

    angular.module('adscan')
        .controller('SysadminController', SysadminController);

    function SysadminController($scope, $rootScope, $cookies, toastr, $http, url) {
        var vm = $scope;
        var me = this;

        vm.displayed = [];
        vm.rowCollection = [];

        vm.activeRow = null
        vm.activeIndex = null

        vm.formdata = {};

        vm.gids = []
        var inputs_zh = [
            { id: 'name', text: '用户名', isText: true },
            { id: 'password', text: '密码', isText: true },
            { id: 'gid', text: '客户名称', isSelect: true },
            { id: 'role', text: '是否管理员', isBox: true },
            { id: 'cpp_media_penetration', text: '媒体渗透率', isBox: true },
            { id: 'cpp_search_engines', text: '搜索引擎', isBox: true },
            { id: 'cpp_ad_penetration', text: '各电商网站广告位渗透率', isBox: true },
            { id: 'cpp_website_penetration', text: '各电商网站渗透率', isBox: true },
            { id: 'cpp_search_key_penetration', text: '站内搜索关键词渗透率', isBox: true },
            { id: 'cpp_search_brand_penetration', text: '站内搜索品牌渗透率', isBox: true },
            { id: 'cpp_shop_path', text: '店内路径', isBox: true },
            { id: 'cpp_shop_penetration', text: '旗舰店分销店渗透率', isBox: true },
            { id: 'crossreach_online_tv', text: 'Online Tv', isBox: true },
            { id: 'crossreach_pc_mobile', text: 'PC Mobile', isBox: true },
            { id: 'dmp', text: '用户筛选分析', isBox: true },
            { id: 'mediaplanning_demographic', text: '人口属性', isBox: true },
            { id: 'mediaplanning_web_habits', text: '上网习惯', isBox: true },
            { id: 'mediaplanning_media_habits', text: '媒介习惯', isBox: true }
        ]
        var inputs_en = [
            { id: 'name', text: 'User Name', isText: true },
            { id: 'password', text: 'Password', isText: true },
            { id: 'gid', text: 'GID', isSelect: true },
            { id: 'role', text: 'Is Admin', isBox: true },
            { id: 'cpp_media_penetration', text: '媒体渗透率', isBox: true },
            { id: 'cpp_search_engines', text: '搜索引擎', isBox: true },
            { id: 'cpp_ad_penetration', text: '各电商网站广告位渗透率', isBox: true },
            { id: 'cpp_website_penetration', text: '各电商网站渗透率', isBox: true },
            { id: 'cpp_search_key_penetration', text: '站内搜索关键词渗透率', isBox: true },
            { id: 'cpp_search_brand_penetration', text: '站内搜索品牌渗透率', isBox: true },
            { id: 'cpp_shop_path', text: '店内路径', isBox: true },
            { id: 'cpp_shop_penetration', text: '旗舰店分销店渗透率', isBox: true },
            { id: 'crossreach_online_tv', text: 'Online Tv', isBox: true },
            { id: 'crossreach_pc_mobile', text: 'PC Mobile', isBox: true },
            { id: 'dmp', text: 'DMP', isBox: true },
            { id: 'mediaplanning_demographic', text: 'Demographic', isBox: true },
            { id: 'mediaplanning_web_habits', text: 'Web-surfing habits', isBox: true },
            { id: 'mediaplanning_media_habits', text: 'Media habits', isBox: true }
        ]
        vm.inputs = $rootScope.isEn ? inputs_en : inputs_zh


        formdataInit()

        getUser();
        getGid();

        vm.clickAdd = function() {
            me.isShowCover = true;
            vm.isNew = 'yes'
        }
        vm.saveUser = function() {
            if (vm.isNew == 'yes') {
                if (isHaveUser(vm.formdata.name)) {
                    alert(!$rootScope.isEn ? '用户已存在' : 'The user already exists')
                    return;
                }
            }

            if (!vm.formdata.gid) {
                alert(!$rootScope.isEn ? '请选择广告客户' : 'Please choice GID')
                return;
            }
            var params = {
                op: 'saveUser',
                token: $cookies.getObject('userInfo').token
            }

            _.forOwn(vm.formdata, function(v, k) {
                if (_.isBoolean(v)) {
                    if (v) {
                        params[k] = '1';
                        if (k == 'role') {
                            params[k] = 'admin'
                        }
                    } else {
                        params[k] = '0';
                        if (k == 'role') {
                            params[k] = ''
                        }
                    }

                }
                if (_.isString(v) || _.isNumber(v)) {
                    params[k] = v || '0'
                }
            })

            params.usermgr='0'

            $http({
                url: url.api2,
                params: params
            }).then(function(data) {
                console.log(data.data)

                if (data.data.status == 200) {
                    me.isShowCover = false
                    toastr.success('提交成功')
                    getUser();
                    formdataInit()

                } else {
                    toastr.success(data.data.error)
                }

            })
        }

        function isHaveUser(name) {
            if (!vm.rowCollection.length) {
                return false;
            }
            return _.some(vm.rowCollection, function(v) {
                return v.name === name
            })
        }

        vm.choice = function(row, inx) {
            vm.activeRow = row
            vm.activeIndex = inx
        }

        vm.edit = function() {
            vm.isNew = 'no'
            var obj = angular.copy(vm.activeRow)
            vm.formdata = obj
            _.forOwn(obj, function(v, k) {
                if (v === '0') {
                    vm.formdata[k] = false
                }

                if (v === '1') {
                    vm.formdata[k] = true
                }

                if (k == 'name' && v == 'admin') {
                    vm.formdata[k] = 'admin'
                }
                if(obj.role=='admin'){
                    vm.formdata.role=true
                }
            })
            me.isShowCover = true
        }

        vm.delUser = function() {
            var params = {
                op: 'destroy_user',
                token: $cookies.getObject('userInfo').token
            }

            params.name = vm.activeRow.name
            $http({
                url: url.api2,
                params: params
            }).then(function(data) {
                if (data.data.status == 200) {
                    console.log(data.data)
                    toastr.success('删除成功')
                    getUser();
                    formdataInit()

                    vm.activeRow = null
                    vm.activeIndex = null
                } else {
                    toastr.success(data.data.error)
                }

            })
        }

        vm.cancel = function() {
            formdataInit()
            me.isShowCover = false
        }

        function formdataInit() {
            vm.inputs.forEach(function(o) {
                if (o.isBox) {
                    vm.formdata[o.id] = 0
                } else {
                    vm.formdata[o.id] = ''
                }

            })
        }

        function getUser() {
            $http({
                url: url.api2,
                method: 'GET',
                params: {
                    op: 'usersListSvr',
                    token: $cookies.getObject('userInfo').token
                }
            }).then(function(data) {
                vm.rowCollection = data.data.data.reverse()
                vm.activeRow = null
                vm.activeIndex = null
            });
        }


        function getGid() {
            $http({
                url: url.api2,
                method: 'GET',
                params: {
                    op: 'listGID',
                    token: $cookies.getObject('userInfo').token,
                }
            }).then(function(data) {
                vm.gids = data.data.data

            });
        }

    }

})();
