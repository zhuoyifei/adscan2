(function() {
  'use strict';

  angular.module('adscan')
    .controller('CppController', CppController);

  /** @ngInject */
  function CppController($cookies) {
    var vm = this;
    var frame = document.querySelector("#cppIframe");
      vm.userInfo = $cookies.getObject('userInfo')
    var links = [{
      "name": "了解-站外搜索",
      "children": [{
        "name": "媒体渗透率",
        "id": "cpp_media_penetration",
        "link": "https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^2-^4e86^^89e3^-^7ad9^^5916^^641c^^7d22^^2f^^5a92^^4f53^^6e17^^900f^^7387^.db&browserType=Chrome"
      }, {
        "name": "搜索引擎",
        "id": "cpp_search_engines",
        "link": "https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^2-^4e86^^89e3^-^7ad9^^5916^^641c^^7d22^^2f^^641c^^7d22^^5f15^^64ce^.db&browserType=Chrome"
      }]
    }, {
      "name": "认可-进入电商",
      "children": [{
        "name": "各电商网站广告位渗透率",
        "id": "cpp_ad_penetration",
        "link": "https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^3-^8ba4^^53ef^-^8fdb^^5165^^7535^^5546^^2f^^5404^^7535^^5546^^7f51^^7ad9^^5e7f^^544a^^4f4d^^6e17^^900f^^7387^.db&browserType=Chrome"
      }, {
        "name": "各电商网站渗透率",
        "id": "cpp_website_penetration",
        "link": "https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^3-^8ba4^^53ef^-^8fdb^^5165^^7535^^5546^^2f^^5404^^7535^^5546^^7f51^^7ad9^^6e17^^900f^^7387^.db&browserType=Chrome"
      }]
    }, {
      "name": "考虑-站内搜索",
      "children": [{
        "name": "站内搜索关键词渗透率",
        "id": "cpp_search_key_penetration",
        "link": "https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^4-^8003^^8651^-^7ad9^^5185^^641c^^7d22^^2f^^7ad9^^5185^^641c^^7d22^^5173^^952e^^8bcd^^6e17^^900f^^7387^.db&browserType=Chrome"
      }, {
        "name": "站内搜索品牌渗透率",
        "id": "cpp_search_brand_penetration",
        "link": "https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^4-^8003^^8651^-^7ad9^^5185^^641c^^7d22^^2f^^7ad9^^5185^^641c^^7d22^^54c1^^724c^^6e17^^900f^^7387^.db&browserType=Chrome"
      }]
    }, {
      "name": "意向-进入店铺",
      "children": [{
        "name": "店内路径",
        "id": "cpp_shop_path",
        "link": "https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^5-^610f^^5411^-^8fdb^^5165^^5e97^^94fa^^2f^^5e97^^5185^^8def^^5f84^.db&browserType=Chrome"
      }, {
        "name": "旗舰店分销店渗透率",
        "id": "cpp_shop_penetration",
        "link": "https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^5-^610f^^5411^-^8fdb^^5165^^5e97^^94fa^^2f^^65d7^^8230^^5e97^^5206^^9500^^5e97^^6e17^^900f^^7387^.db&browserType=Chrome"
      }]
    }, {
      "name": "购买-购物订单",
      "children": [{
        "name": "购买用户",
        "link": "https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^6-^8d2d^^4e70^-^8d2d^^7269^^8ba2^^5355^^2f^^8d2d^^4e70^^7528^^6237^.db&browserType=Chrome"
      }, {
        "name": "丢失用户",
        "link": "https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^6-^8d2d^^4e70^-^8d2d^^7269^^8ba2^^5355^^2f^^4e22^^5931^^7528^^6237^.db&browserType=Chrome"
      }]
    }];
    vm.links = links.map(function(p){
      p.isShowTitle=p.children.some(function(c){
        return vm.userInfo[c.id]=='1' || vm.userInfo.role=='admin'
      })
      return p
    });
    vm.activeLink = links[0].children[0];
    frame.setAttribute("src", vm.activeLink.link);
    vm.changeLink = function(l) {
      if (vm.activeLink.name === l.name) return;
      vm.activeLink = l;
      frame.setAttribute("src", vm.activeLink.link);

    }
  }

  /**
   var links={
  "了解-站外搜索":{
    "媒体渗透率":"https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^2-^4e86^^89e3^-^7ad9^^5916^^641c^^7d22^^2f^^5a92^^4f53^^6e17^^900f^^7387^.db&browserType=Chrome",
    "搜索引擎":"https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^2-^4e86^^89e3^-^7ad9^^5916^^641c^^7d22^^2f^^641c^^7d22^^5f15^^64ce^.db&browserType=Chrome"
  },
  "认可-进入电商":{
    "各电商网站广告位渗透率":"https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^3-^8ba4^^53ef^-^8fdb^^5165^^7535^^5546^^2f^^5404^^7535^^5546^^7f51^^7ad9^^5e7f^^544a^^4f4d^^6e17^^900f^^7387^.db&browserType=Chrome",
    "各电商网站渗透率":"https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^3-^8ba4^^53ef^-^8fdb^^5165^^7535^^5546^^2f^^5404^^7535^^5546^^7f51^^7ad9^^6e17^^900f^^7387^.db&browserType=Chrome"
  },
  "考虑-站内搜索":{
    "站内搜索关键词渗透率":"https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^4-^8003^^8651^-^7ad9^^5185^^641c^^7d22^^2f^^7ad9^^5185^^641c^^7d22^^5173^^952e^^8bcd^^6e17^^900f^^7387^.db&browserType=Chrome",
    "站内搜索品牌渗透率":"https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^4-^8003^^8651^-^7ad9^^5185^^641c^^7d22^^2f^^7ad9^^5185^^641c^^7d22^^54c1^^724c^^6e17^^900f^^7387^.db&browserType=Chrome"
  },
  "意向-进入店铺":{
    "店内路径":"https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^5-^610f^^5411^-^8fdb^^5165^^5e97^^94fa^^2f^^5e97^^5185^^8def^^5f84^.db&browserType=Chrome",
    "旗舰店分销店渗透率":"https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^5-^610f^^5411^-^8fdb^^5165^^5e97^^94fa^^2f^^65d7^^8230^^5e97^^5206^^9500^^5e97^^6e17^^900f^^7387^.db&browserType=Chrome"
  },
  "购买-购物订单":{
    "购买用户":"https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^6-^8d2d^^4e70^-^8d2d^^7269^^8ba2^^5355^^2f^^8d2d^^4e70^^7528^^6237^.db&browserType=Chrome",
    "丢失用户":"https://comscan.comsmartgroup.com/?proc=1&action=viewer&hback=true&db=^6570^^636e^^7ba1^^7406^^5e73^^53f0^^2f^1-ComTracker^7528^^6237^^753b^^50cf^^5206^^6790^^2f^^7f51^^8d2d^^6f0f^^6597^^2f^6-^8d2d^^4e70^-^8d2d^^7269^^8ba2^^5355^^2f^^4e22^^5931^^7528^^6237^.db&browserType=Chrome"
  }
}

   var list=Object.keys(links).map(k=>{
  return {
    name:k,
    children:Object.keys(links[k]).map(i=>{
      return {
        name:i,
        link:links[k][i]
      }
    })
  }
})

   **/
})();