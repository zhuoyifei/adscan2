(function() {
    'use strict';

    angular
        .module('adscan')
        .controller('PcMobileController', PcMobileController);

    /** @ngInject */
    function PcMobileController($rootScope, $q, $http, FileUploader, url, $scope, $log, Resource) {
        var vm = this
        var userInfo = $rootScope.userInfo
        var dlData = null;
        var initData = null

        // vm.isShowRun = true
        // vm.isShowSaveAs=true
        var summary01Data = null;
        var summary02Data = null;
        var summary03Data = null;

        var summary01DataA, summary01DataB, summary01DataC, styleXY;

        vm.cbv = vm.arv = 50.00


        vm.itemsByPage = 5
        vm.rowCollection = vm.displayedCollection = []

        listReport()
        loadData()

        var upurl = "http://data.ad-rating.com:9090/adscangrp/api.jsp?op=upload";
        var uploader = vm.uploader = new FileUploader({
            url: upurl
        })

        uploader.onAfterAddingFile = function(fileItem) {
            $log.log('typeof', fileItem._file)
            xhrupload(fileItem._file)
            fileItem.remove()
        };


        vm.add = function() {
            cfgInit()
            vm.isShowCover = true
        }

        vm.run = function() {
            if (!isHaveActiveRow()) {
                alert('未选中报表');
                return false;
            }

            vm.cfg = angular.fromJson(vm.lastActiveRow.config)

            vm.cfg.ovcpm = parseFloat(vm.cfg.ovcpm)
            $log.log('run ', vm.cfg)
            getInitData(vm.cfg)

            vm.reportName = vm.lastActiveRow.name
            vm.tabIndex = 0
        }

        vm.addrun = function() {
            vm.isShowRun = true
            vm.isShowCover = false;
            getInitData(vm.cfg)
        }
        vm.edit = function() {
            if (!isHaveActiveRow()) {
                alert('未选中报表');
                return false;
            }
            var ac_cfg = JSON.parse(vm.lastActiveRow.config)
            $log.log(vm.lastActiveRow, vm.lastActiveRow.id)
            ac_cfg.ovcpm = parseFloat(ac_cfg.ovcpm)
            vm.cfg = ac_cfg

            vm.isShowCover = true
        }

        vm.editTab = function(inx) {
            vm.isShowCover = true
            vm.addActiveIndex = inx || 0;
        }


        vm.delReprt = function() {
            var ids = activeReport()
            if (!isHaveActiveRow()) {
                alert('未选中报表');
                return false;
            }
            if (window.confirm('确定删除所选项')) {
                Resource.getKpiData('gaeDeleteReport', { id: ids.join(','), token: userInfo.token }).then(function(data) {
                    if (data.status == 200) {
                        listReport()
                    }
                })
            }
        }

        vm.cancel = function() {
            cfgInit()
            vm.isShowCover = false
        }
        vm.saveAs = function() {
            vm.save(vm.saveAsName)
            vm.isShowSaveAs = false
        }

        vm.save = function(n) {
            var data = {
                "type": "pcmobile",
                "name": n || vm.reportName,
                "ad2token": userInfo.ad2token,
                "config": angular.toJson(vm.rcfg)
            }
            $http({
                url: url.api + '?op=gaeSaveReport&token=' + userInfo.token,
                method: "POST",
                data: data,
            }).then(function(data) {
                $log.log(data.data)
                alert($rootScope.isEn ? 'Save Suucess' : '保存成功')
                listReport()
            })
        }

        vm.downloadRpt = function(isFull) {
            if (!dlData) {
                alert('数据不全')
                return false;
            }
            var index =  !isFull ?  vm.tabIndex : '-1'
            var currentJson = initData,
                jsondata;
            var config = vm.cfg;
            currentJson.config = { age: config.ageFrom + "-" + config.ageTo, gender: config.genderText, mpi: config.mpiText, city: config.cityText, styleXY: styleXY };
            jsondata = JSON.stringify(currentJson);
            $http({
                url: url.api + '?op=ad3_download_excel&index=' + index,
                method: "POST",
                data: { data: jsondata, ad2token: userInfo.ad2token },
            }).then(function(data) {
                $log.log(data.data)
                downloadFile(data.data.url)
            }, function(er) {
                alert('download fail!')
            })
        }

        function downloadFile(url) {
            var a = document.createElement("a");
            a.href = url;
             // a.setAttribute('target',"_blank")
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        }
        vm.test = function() {
            vm.isShowCover = true
            cfgInit()
        }

        vm.rowClick = function(o) {
            if (o.isSelected) {
                vm.lastActiveRow = o
                $log.log('last is this' + o.id)
            }
        }

        vm.render = function() {
            t1_grp()
            t1_spending()

            t2_grp()
            t2_spending()

            t3_ba()
            t4()


        }

        vm.render1 = function() {
            if (!initData) {
                return false;
            }
            setTimeout(function() {
                t1_grp()
                t1_spending()
            }, 0)
        }
        vm.render2 = function() {
            if (!initData) {
                return false;
            }

            setTimeout(function() {
                t2_grp()
                t2_spending()
            }, 0)
        }
        vm.render3 = function() {
                if (!initData) {
                    return false;
                }
                setTimeout(function() {
                    t3_ba()
                }, 0)

            }
            // setTimeout(function() {
            //     vm.render()
            // }, 0)
        vm.t4 = function() {
            t4()
        }
        vm.tab2_levelChange = function() {
            t2_grp()
            t2_spending()
        }

        vm.tab3_levelChange = function() {
            t3_ba()
        }


        function t1_grp() {
            var data = initData.data
            var option = {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'GRP vs. Audience Reach %'
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: getColumn(data, "grp"),
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: "Audience Reach %"
                    },
                    labels: {
                        formatter: function() {
                            return this.value + ' %';
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Reach 1+ %',
                    data: getColumn(data, "total_rc1")
                }, {
                    name: 'Reach 2+ %',
                    data: getColumn(data, "total_rc2")
                }, {
                    name: 'Reach 3+ %',
                    data: getColumn(data, "total_rc3")
                }, {
                    name: 'Reach 4+ %',
                    data: getColumn(data, "total_rc4")
                }]
            };
            Highcharts.chart("hc-ga1", option);

            vm.tb_ga1 = getTableData(data, ['grp', 'total_rc1', 'total_rc2', 'total_rc3', 'total_rc4'])
        }

        function t1_spending() {
            var data = initData.data
            var option = {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Spending vs. Audience Reach %'
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: getColumn(data, "spending"),
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: "Audience Reach %"
                    },
                    labels: {
                        formatter: function() {
                            return this.value + ' %';
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Reach 1+ %',
                    data: getColumn(data, "total_rc1")
                }, {
                    name: 'Reach 2+ %',
                    data: getColumn(data, "total_rc2")
                }, {
                    name: 'Reach 3+ %',
                    data: getColumn(data, "total_rc3")
                }, {
                    name: 'Reach 4+ %',
                    data: getColumn(data, "total_rc4")
                }]
            };

            Highcharts.chart("hc-spending1", option)

            vm.tb_spending1 = getTableData(data, ['spending', 'total_rc1', 'total_rc2', 'total_rc3', 'total_rc4'])
        }

        function t2_grp() {
            var data = initData.data
            var level = vm.tab2_level
            var option = {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'GRP vs. Audience Reach %'
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: getColumn(data, "grp"),
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: "Audience Reach %"
                    },
                    labels: {
                        formatter: function() {
                            return this.value + ' %';
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Total',
                    data: getColumn(data, "total_rc" + level),
                }, {
                    name: 'PC',
                    data: getColumn(data, "tv_rc" + level),
                }, {
                    name: 'Mobile',
                    data: getColumn(data, "online_rc" + level),
                }]
            };
            Highcharts.chart("hc-ga2", option);

            vm.tb_ga2 = getTableData(data, [
                'grp',
                'total_rc' + level,
                'tv_rc' + level,
                'online_rc' + level
            ])
        }

        function t2_spending() {
            var data = initData.data
            var level = vm.tab2_level

            var option = {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: 'Spending vs. Audience Reach %'
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: getColumn(data, "spending"),
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: "Audience Reach %"
                    },
                    labels: {
                        formatter: function() {
                            return this.value + ' %';
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Total',
                    data: getColumn(data, "total_rc" + level),
                }, {
                    name: 'PC',
                    data: getColumn(data, "tv_rc" + level),
                }, {
                    name: 'Mobile',
                    data: getColumn(data, "online_rc" + level),
                }]
            };

            Highcharts.chart("hc-spending2", option)

            vm.tb_spending2 = getTableData(data, [
                'spending',
                'total_rc' + level,
                'tv_rc' + level,
                'online_rc' + level
            ])
        }

        function t3_ba() {
            var data = initData.t3
            console.log('t3', data)
            var level = vm.tab3_level
            for (var i = 0; i < data.length; i++) {
                var t = data[i];
                t["a"] = t["total_aud_rc"];
                t["b"] = t["tv_spend"];
                t["c"] = t["online_spend"];
            }
            var option = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Budget Allocation Optimization'
                },
                subtitle: {
                    text: null
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: getColumn(data, "a", function(d) {
                        return d.type == level
                    }),
                    title: {
                        text: 'Maximum Audience Reach ' + level + '+ %'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: "Minimum Media Spending (RMB)"
                    },
                    labels: {
                        formatter: function() {
                            return this.value;
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        stacking: 'normal'
                    }
                },
                series: [{
                    name: 'PC',
                    data: getColumn(data, "b", function(d) {
                        return d.type == level
                    })
                }, {
                    name: 'Mobile',
                    data: getColumn(data, "c", function(d) {
                        return d.type == level
                    })
                }]
            };

            Highcharts.chart("hc-tab3", option)

        }


        function getTableData(dar, nar) {
            var re = []
            dar.forEach(function(o) {
                re.push(nar.map(function(n) {
                    return o[n]
                }))
            })
            return re
        }

        function t4() {
            var level = vm.tab4_level
            var sa = Resource.getKpiData('summarymobile01a', { flevel: level })
            var sb = Resource.getKpiData('summarymobile01b', { flevel: level })
            var sc = Resource.getKpiData('summarymobile01c', { flevel: level })
            var s2 = Resource.getKpiData('summarymobile02')
            var s3 = Resource.getKpiData('summarymobile03')
            $q.all([sa, sb, sc, s2, s3]).then(function(vals) {
                $log.log(vals)
                summary01DataA = vals[0]
                summary01DataB = vals[1]
                summary01DataC = vals[2]

                summary02Data = vals[3]
                summary03Data = vals[4]
                getT4data()
            })
        }

        vm.tab4_levelChange = function() {
            t4()
        }

        function getT4data() {
            var re2 = fixsummary02Data()
            var re3 = fixsummary03Data()
            vm.t4tb = re2
            vm.t4tc = re3

            vm.t4ta = vm.isCB ? changeStyleMediaSpending() : changeStyleAudienceReach()
            vm.isCBTab = vm.isCB
            $log.log("t4ta", vm.isCB, vm.t4ta)
            $log.log('styleXY', styleXY)
        }



        function fixsummary02Data() {
            var level = vm.tab4_level
            var arr = getType("rc" + level);
            for (var i = 0; i < arr.length; i++) {
                var t = summary02Data[i + 2];
                for (var j = 0; j < 21; j++) {
                    // t["b" + (j + 2)] = Math.round(arr[i]["f" + j]); //100;
                      var res=/^\d*\.\d/.exec(arr[i]["f" + j]+'')
                    t["b" + (j + 2)] =_.slice(res)[0]
                }
            }
            var config = vm.cfg;
            summary02Data[0]["b0"] = config.cityText;
            var re = []

            re[0] = [summary02Data[0].b0, summary02Data[0].b2]

            summary02Data.slice(1).forEach(function(v, inx) {
                if (inx < 2) {
                    var ar = new Array(23)
                    _.forOwn(v, function(value, key) {
                        ar[parseInt(key.substring(1))] = typeof value == 'string' ? value.replace('null', level) : value
                    })
                    re.push(ar)
                } else {
                    var ar = new Array(22)
                    _.forOwn(v, function(value, key) {
                        ar[parseInt(key.substring(1)) - 1] = value
                    })
                    re.push(ar)
                }
            });

            return re

        }

        function fixsummary03Data() {
            var arr = getType("total_spend")
            for (var i = 0; i < arr.length; i++) {
                var t = summary03Data[i + 2];
                for (var j = 0; j < 21; j++) {
                    t["c" + (j + 2)] = Math.round(arr[i]["f" + j] / 1000);
                }
            }
            var config = vm.cfg;
            summary03Data[0]["c0"] = config.cityText;

            var re = []

            re[0] = [summary03Data[0].c0, summary03Data[0].c2]

            summary03Data.slice(1).forEach(function(v, inx) {
                if (inx < 2) {
                    var ar = new Array(23)
                    _.forOwn(v, function(value, key) {
                        ar[parseInt(key.substring(1))] = value
                    })
                    re.push(ar)
                } else {
                    var ar = new Array(22)
                    _.forOwn(v, function(value, key) {
                        ar[parseInt(key.substring(1)) - 1] = value
                    })
                    re.push(ar)
                }
            });

            return re
        }

        function changeStyleMediaSpending() {
            if (summary03Data == null) return;
            var summary01Data = summary01DataA.slice(0);
            var v = vm.cbv || 50;
            v = parseFloat(v);
            styleXY = [];
            for (var j = 2; j <= 22; j++) {
                var p = 2;
                var min = 99999999;
                for (var i = 2; i < summary03Data.length; i++) {
                    var t = summary03Data[i];

                    var c = Math.abs(t["c" + j] * 1 - v);

                    if (min >= c) {
                        p = i;
                        min = c;
                    }
                }
                styleXY.push([p, j]);
            }

            for (var i = styleXY.length - 1; i > -1; i--) {
                var p = styleXY[i];
                var row = summary03Data[p[0]];
                //计算ms total
                summary01Data[0]["a" + (i + 2)] = summary03Data[p[0]]["c" + p[1]];
                //计算ms tv
                summary01Data[1]["a" + (i + 2)] = summary03Data[p[0]]["c2"];
                //计算ms online
                summary01Data[2]["a" + (i + 2)] = summary03Data[2]["c" + p[1]];
                //计算grp tv
                summary01Data[3]["a" + (i + 2)] = summary03Data[p[0]]["c1"];
                //计算grp online
                summary01Data[4]["a" + (i + 2)] = summary03Data[1]["c" + p[1]];
                //计算grp total
                summary01Data[5]["a" + (i + 2)] = summary01Data[3]["a" + (i + 2)] * 1 + summary01Data[4]["a" + (i + 2)] * 1;
                //计算ar
                summary01Data[6]["a" + (i + 2)] = Math.round(summary02Data[p[0]]["b" + p[1]] * 100) / 10000;

            }
            console.log('summary01Data Campaign Budget ', summary01Data);
            // return summary01Data
            var re = []
            summary01Data.forEach(function(o, inx) {
                var ar = []
                _.forOwn(o, function(value, key) {
                    ar[parseInt(key.substring(1))] = value
                })

                re.push(ar[0] ? ar : ar.slice(1))
            })

            return re

        }

        function changeStyleAudienceReach() {
            if (summary02Data == null) return;

            var summary01Data = summary01DataC.slice(0);
            var v = vm.arv || 50;
            v = parseFloat(v);
            styleXY = [];

            for (var j = 2; j <= 22; j++) {
                var p = 2;
                var min = 99999999;
                for (var i = 2; i < summary02Data.length; i++) {
                    var t = summary02Data[i];

                    var c = Math.abs(t["b" + j] * 1 - v);

                    if (min >= c) {
                        p = i;
                        min = c;
                    }
                }
                styleXY.push([p, j]);
            }

            for (var i = styleXY.length - 1; i > -1; i--) {
                var p = styleXY[i];
                var row = summary03Data[p[0]];
                //计算ar
                summary01Data[0]["a" + (i + 2)] = summary02Data[p[0]]["b" + p[1]] + "%";
                //计算grp tv
                summary01Data[1]["a" + (i + 2)] = summary03Data[p[0]]["c1"];
                //计算grp online
                summary01Data[2]["a" + (i + 2)] = summary03Data[1]["c" + p[1]];
                //计算grp total
                summary01Data[3]["a" + (i + 2)] = summary01Data[1]["a" + (i + 2)] * 1 + summary01Data[2]["a" + (i + 2)] * 1;
                //计算ms tv
                summary01Data[4]["a" + (i + 2)] = summary03Data[p[0]]["c2"];
                //计算ms online
                summary01Data[5]["a" + (i + 2)] = summary03Data[2]["c" + p[1]];
                //计算ms total
                summary01Data[6]["a" + (i + 2)] = summary03Data[p[0]]["c" + p[1]];

            }
            console.log('summary01Data  Audience Reach ', summary01Data);

            // return summary01Data

            var re = []
            summary01Data.forEach(function(o, inx) {
                var ar = []
                _.forOwn(o, function(value, key) {
                    ar[parseInt(key.substring(1))] = value
                })

                re.push(ar[0] ? ar : ar.slice(1))
            })

            return re
        }




        function getType(type) {
            var currentSummary = initData.summary;
            var arr = [];
            for (var i = 0; i < currentSummary.length; i++) {
                var t = currentSummary[i];
                if (t.type == type)
                    arr.push(t)
            }
            return arr;
        }




        function loadData() {
            vm.durations = [
                { id: "2", text: '2 Weeks' },
                { id: "3", text: '3 Weeks' },
                { id: "4", text: '4 Weeks' },
                { id: "8", text: '8 Weeks' },
                { id: "12", text: '12 Weeks' },
            ];

            vm.ageFromArray = _.map([15, 20, 25, 30, 35, 40, 45, 50, 55, 60], function(k) {
                return k + ''
            });
            vm.ageToArray = _.map(vm.ageFromArray, function(k) {
                return k * 1 + 4 + ''
            });

            Resource.getData('ad3_dic_mpi').then(function(data) {
                vm.mpiList = data
                vm.mpiMap = ar_to_map(data, 'id')
            })

            Resource.getData('ad3_dic_city').then(function(data) {
                vm.cityList = data
                vm.cityMap = ar_to_map(data, 'id')
            })
            Resource.getData('ad3_dic_mobile', { city: '0' }).then(function(data) {
                vm.netList = data
                vm.netMap = ar_to_map(data, 'id')
            })
            Resource.getData('ad2_dic_ovsite').then(function(data) {
                vm.siteList = data
                vm.siteMap = ar_to_map(data, 'id')
            })
        }

        function ar_to_map(ar, key) {
            var re = {}
            ar.forEach(function(o) {
                re[o[key]] = o
            })

            return re
        }

        function cfgInit() {
            vm.upfile = null
            vm.addActiveIndex = 0
            vm.cfg = {
                "campaignDuration": "2",
                "ageFrom": '15',
                "ageTo": '19',
                "genderMale": false,
                "genderFemale": false,
                "mpi": [],
                "city": '0',
                "tvnetwork": [],
                "ovcpm": 0,
                "ovsites": [],
                "uploadFileID": null
            }

        }

        function isHaveActiveRow() {
            return !!activeReport().length
        }

        function activeReport() {
            var ids = []
            _.forEach(vm.rowCollection, function(o) {
                if (o.isSelected) {
                    ids.push(o.id)
                }
            })
            return ids
        }

        function listReport() {
            Resource.getKpiData('reportsJson', { type: "pcmobile", token: userInfo.token }).then(function(data) {
                vm.rowCollection = data.data
            })
        }

        function xhrupload(file) {

            var formData = new FormData();
            formData.append('file', file, file.name);
            var xhr = new XMLHttpRequest();
            xhr.open('POST', url.api + '?op=upload', true);
            xhr.send(formData);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    var json = JSON.parse(xhr.responseText);
                    console.log('upload json', json)
                    checkExcel(json);
                    uploader.clearQueue()

                } else {
                    alert('fail upload')
                }
            };


        }

        function checkExcel(json) {
            $http({
                type: "GET",
                url: url.api + "?op=checkExcel&name=" + json.name,
                dataType: "json",
            }).then(function(data) {
                $log.log('checkExcel', data.data)
                vm.cfg.uploadFileID = data.data.name
            }, function(er) {
                alert('excel formate error')
            })
        }

        function getInitData(cfg) {
            vm.rcfg = null

            cfg = angular.copy(cfg)
            cfg.campaignDurationText = cfg.campaignDuration + " Weeks";
            cfg.genderText = '';
            cfg.genderText += cfg.genderMale ? 'Male' : '';
            cfg.genderText += ' ';
            cfg.genderText += cfg.genderFemale ? 'Female' : '';

            $log.log(cfg.genderText)
            cfg.mpiText = cfg.mpi.map(function(k) {
                return vm.mpiMap[k].text
            }).join(',')

            cfg.cityText = vm.cityMap[cfg.city].text;

            cfg.tvnetworkText = cfg.tvnetwork.map(function(k) {
                return vm.netMap[k].text
            }).join(',')


            cfg.ovsitesText = cfg.ovsites.map(function(k) {
                return vm.siteMap[k].text
            }).join(',')

            cfg.ovcpm = cfg.ovcpm + ''

            vm.rcfg = cfg
            vm.reportName = getDefaultReportName(vm.rcfg)
            $log.log('reportName', vm.reportName)
            $http({
                url: url.api + '?op=ad3_init_data',
                method: 'POST',
                data: {
                    uploadFileID: cfg.uploadFileID,
                    config: angular.toJson(cfg),
                    ad2token: userInfo.ad2token
                }
            }).then(function(data) {
                $log.log('init data', data.data)
                initData = dlData = data.data
                console.log('initData', typeof initData)
                if (typeof initData === 'string' || !initData.data.length || !initData.summary.length || !initData.t3.length) {
                    alert('data load fail')
                } else {
                    vm.isShowRun = true
                    vm.isShowCover = false;
                    vm.render()
                }
            }, function(er) {
                alert('init data fail!')
            })
        }

        function getDefaultReportName(c) {
            return c.cityText + "_" + c.ageFrom + "-" + c.ageTo + "_" + c.genderText + (c.genderText.length > 2 ? "_" : "") + c.mpiText + "_" + c.campaignDurationText;
        }

        function getHcData(ar) {

        }

        function getColumn(objArr, name, filterFunc) {
            var arr = [];
            for (var i = 0; i < objArr.length; i++) {
                var t = objArr[i];
                if (filterFunc != null) {
                    if (filterFunc(t)) {
                        arr.push(t[name]);
                    }
                } else
                    arr.push(t[name]);
            }
            return arr;
        }

    }
})();
