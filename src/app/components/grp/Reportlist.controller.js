(function() {
    'use strict';

    angular
        .module('adscan')
        .controller('ReportlistController', ReportlistController);

    function ReportlistController($scope,$timeout, $rootScope, $filter, url, listTypeSvc, $http, Resource, $state) {
        var vm = $scope;
        var titles = { tp: '广告曝光受众分析', ap: '受众属性报表', rc: '行业对标分析' }
        var titles_en = { tp: 'Target Performance Report', ap: 'Audience Profile Report', rc: 'Reach Curve Report' }
        var types = { tp: 'TargetPerformance', ap: 'AudienceProfile', rc: 'ReachCurve' }
        vm.title =$rootScope.isEn ? titles_en[listTypeSvc] : titles[listTypeSvc];
        vm.rowCollection = [];

        vm.activeRow = null
        vm.activeIndex = null

        vm.displayed = [];
        vm.rowCollection = [];

        getData(types[listTypeSvc])

        vm.run = function() {
            vm.info = '运行中'
            $http.get(url.api + '?op=runRPTByID&id=' + vm.activeRow.id).then(function(data) {
                console.log(data)
                vm.info = '运行成功';
                downloadFile(data.data.url)
                $timeout(function(){
                    vm.info=''
                 },2000)
            },function(er){
                 vm.info = '运行失败';
                 $timeout(function(){
                    vm.info=''
                 },2000)
            })

        }

        vm.edit = function() {
            $state.go('home.grp.' + listTypeSvc, { id: vm.activeRow.id })
        }
        vm.del = function() {
            $http.get(url.api + '?op=delReport&id=' + vm.activeRow.id + '&token=' + $rootScope.userInfo.token).then(function(data) {
                console.log(data)
                if (data.data.status == 200) {
                    vm.rowCollection.splice(vm.activeIndex, 1)
                    vm.activeRow = null;
                    vm.activeIndex = null;
                } else {
                    alert(data.data.error)
                }
            })


        }

        vm.choice = function(row, inx) {
            vm.activeRow = row
            vm.activeIndex = inx
        }



        function getData(type) {
            Resource.getData('reportListSvr', { cid: type }).then(function(data) {
                vm.rowCollection = data
            })
        }

        function downloadFile(url) {
            var aLink = document.createElement('a');
            aLink.download = url.replace(/^.*[\\\/]/, '');
            aLink.href = url;
            document.body.appendChild(aLink);
            aLink.click();
            document.body.removeChild(aLink);
        }


        //

        //
    }
})();
