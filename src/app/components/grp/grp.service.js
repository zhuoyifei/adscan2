(function() {
    'use strict';

    angular
        .module('adscan')
        .factory('FiltersSvc', FiltersSvc)
        .factory('DimensionsSvc', DimensionsSvc);



    var filtersArray = [{
        name: "Platform",
        model: 'platform',
        label: "广告投放平台",
        type: 'select',
        multiple: false,
        data: [{ id: '1', text: 'Desktop' }, { id: '2', text: 'Mobile' }],
        required: true,
        follower: ['Advertiser'],
    }, {
        name: "GID",
        label: "客户名称",
        type: 'select',
        op: 'listGID',
        multiple: false,
        follower: ['Advertiser', 'Network'],
        required: true,
    }, {
        name: "Network",
        model: 'nid',
        label: "客户ID",
        type: 'select',
        multiple: false,
        op: 'listNetwork',
        follower: ['Advertiser'],
        required: true,
    }, {
        name: "Advertiser",
        label: "广告客户",
        type: 'select',
        multiple: false,
        op: "listAdvertiser",
        follower: ['Campaign'],
        required: true,
        style: "width:60%",
    }, {
        name: "Campaign",
        label: "广告名称",
        type: 'select',
        multiple: false,
        op: "listCampaign",
        required: true,
    }, {
        name: "Province",
        label: "省份",
        type: 'multipleselect',
        multiple: true,
        inverse: 'City',
        isLink: true,
        follower: ['City'],
        op: "listProvince",
    }, {
        name: "City",
        label: "城市",
        type: 'multipleselect',
        multiple: true,
        isLink: true,
        inverse: 'Province',
        op: "listCity",
    }, {
        name: "Site",
        label: "投放媒体",
        type: 'multipleselect',
        multiple: true,
        op: "listSite",
        isLink: true,
        follower: ['Placement'],
    }, {
        name: "Placement",
        label: "投放位置",
        type: 'multipleselect',
        multiple: true,
        isLink: true,
        op: "listPlacement",
    }, {
        name: "Gender",
        label: "受众性别",
        type: 'select',
        multiple: false,
        data: [{ id: '男', text: '男' }, { id: '女', text: '女' }],
    }, {
        name: "Age",
        label: "受众年龄",
        type: 'doubleselect',
        data: [
            [15, 20, 25, 30, 35, 40, 45, 50],
            [19, 24, 29, 34, 39, 44, 49, 99]
        ],
        multiple: false,
        min: 15,
        max: 99,
        serverURL: "media/data/age.json",
    }, {
        name: "MPI",
        label: "受众收入",
        type: 'doubleselect',
        data: [
            [2001, 3001, 5001, 8001, 10001, 15001],
            [3000, 5000, 8000, 10000, 15000, 100000]
        ],
        multiple: false,
    }];

    var filtersCfgMap = {}

    filtersArray.forEach(function(v) {
        filtersCfgMap[v.name] = v
    })

    function FiltersSvc() {
        return {
            filtersArray: filtersArray,
            getMap: function(ar) {
                if(!ar)return filtersCfgMap
                var map = {}
                ar.forEach(function(n) {
                   map[n] = filtersCfgMap[n]
                })
                return map
            }
        }
    }


    //dimensions

    var dimensionsArray = [{
        name: 'Time',
        text: '报告周期',
        inverse: 'TimeCampaign-to-date',
        isHaveData: true,
        data: [{ "id": "11", "text": "Daily" },
            { "id": "12", "text": "Weekly(Sun-Sat)" },
            { "id": "14", "text": "Monthly" }
        ]
    }, {
        name: 'TimeCampaign-to-date',
        text: '累计报告周期',
        inverse: 'Time',
        isHaveData: true,
        data: [{ "id": "21", "text": "Daily(Campaign-to-date)" },
            { "id": "22", "text": "Weekly(Sun-Sat)(Campaign-to-date)" },
            { "id": "24", "text": "Monthly(Campaign-to-date)" }
        ]
    }, {
        name: "City",
        text: "城市",
        inverse:'Province',
        isLink: true
    }, {
        name: "Province",
        text: "省份",
        inverse:'City',
        isLink: true
    }, {
        name: "Site",
        text: "投放媒体",
        isLink: true
    }, {
        name: "Placement",
        text: "投放位置",
        isLink: true
    }, {
        name: "Gender",
        text: "受众性别",
    }, {
        name: "Age",
        text: "受众年龄",
    }, {
        name: "MPI",
        text: "受众收入",
    }];

    var dimensionsCfgMap = {}

    dimensionsArray.forEach(function(o) {
        dimensionsCfgMap[o.name] = o
    });

    function DimensionsSvc() {
        return {
            dimensionsArray: dimensionsArray,
            getMap: function(ar) {
                if(!ar)return dimensionsCfgMap
                var map = {}
                ar.forEach(function(n) {
                   map[n] = dimensionsCfgMap[n]
                })
                return map
            }
        }
    }






})();
