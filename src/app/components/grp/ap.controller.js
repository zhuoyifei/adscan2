(function() {
    'use strict';

    angular
        .module('adscan')
        .controller('ApController', ApController);

    function ApController($scope, RpIdSvc, $rootScope, FiltersSvc, DimensionsSvc, url, $http, Resource) {
        var vm = this;
        vm.title = '受众属性报表';
        vm.sid = []
        vm.metricsType = 'External';

        vm.dateRangeEnd=new Date((new Date()).getTime()-24*60*60*1000)


        //筛选限制

        vm.allFiltersNameArray = [
            "Platform",
            "GID",
            "Network",
            "Advertiser",
            "Campaign",
            "Province",
            "City",
            "Site",
            "Placement"
        ]


        vm.filtersCfgMap = FiltersSvc.getMap(vm.allFiltersNameArray)
            //已选条件
        vm.activeFiltersNameArray = ['Platform', 'GID', 'Network', 'Advertiser', 'Campaign']

        //待选条件
        vm.restFiltersNameArray = _.difference(vm.allFiltersNameArray, vm.activeFiltersNameArray)

        //初始化条件可选数据
        vm.allFiltersNameArray.forEach(function(v) {
            vm[v + 'list'] = []
        })


        //维度

        //维度限制
        vm.allDimensionsNameArray = ["Time", "TimeCampaign-to-date", "City", "Province", "Site", "Placement", 'Gender', 'Age', "MPI"];

        vm.dimensionsCfgMap = DimensionsSvc.getMap(vm.allDimensionsNameArray)
            //已选维度
        vm.activeDimensionsNameArray = []

        //待选维度
        vm.restDimensionsNameArray = _.difference(vm.allDimensionsNameArray, vm.activeDimensionsNameArray)


        vm.addFilter = function(n) {
            var inx = _.indexOf(vm.restFiltersNameArray, n)
            var o = vm.filtersCfgMap[n]
            if (o.inverse) {
                var key = vm.activeFiltersNameArray.indexOf(o.inverse)
                if (key !== -1) {
                    vm.restFiltersNameArray.push(vm.activeFiltersNameArray.splice(key, 1)[0])
                }
            }
            vm.restFiltersNameArray.splice(inx, 1)
            vm.activeFiltersNameArray.push(n)
            if (o.isLink) {
                var key2 = vm.restDimensionsNameArray.indexOf(n)
                if (key2 !== -1) {
                    vm.activeDimensionsNameArray.push(vm.restDimensionsNameArray.splice(key2, 1)[0])
                }
                if (o.inverse) {
                    var key = vm.activeDimensionsNameArray.indexOf(o.inverse)
                    if (key !== -1) {
                        vm.restDimensionsNameArray.push(vm.activeDimensionsNameArray.splice(key, 1)[0])
                    }
                }
            }
        }
        vm.delFilter = function(n) {
            var inx = _.indexOf(vm.activeFiltersNameArray, n)
            vm.restFiltersNameArray.unshift(vm.activeFiltersNameArray.splice(inx, 1)[0])
            _.isArray(vm[n]) ? vm[n] = [] : vm[n] = null;
            vm[n] = vm[n + '1'] = vm[n + '2'] = null;
            var o = vm.filtersCfgMap[n]
            if (o.isLink) {
                var j = vm.activeDimensionsNameArray.indexOf(n)
                if (j !== -1) {
                    vm.restDimensionsNameArray.push(vm.activeDimensionsNameArray.splice(j, 1)[0])
                }
            }
        }
        vm.filterInit = function(o) {
            updateFilterData(o);
        }
        vm.filterChange = function(o) {
            updateParams();
            updateFilterData(o);
        }

        vm.removeItem = function(item, model, n) {
            for (var i = vm[n].length - 1; i >= 0; i--) {
                if (vm[n][i].id == item.id) {
                    vm[n].splice(i, 1)
                    break;
                }
            }
            vm.filterChange(vm.filtersCfgMap[n])
        }

        vm.addDimension = function(n) {
            var inx = _.indexOf(vm.restDimensionsNameArray, n)
            var o = vm.dimensionsCfgMap[n];
            vm.restDimensionsNameArray.splice(inx, 1)
            vm.activeDimensionsNameArray.push(n)

            if (o.inverse) {
                var key = vm.activeDimensionsNameArray.indexOf(o.inverse)
                if (key !== -1) {
                    vm.restDimensionsNameArray.push(vm.activeDimensionsNameArray.splice(key, 1)[0])
                }
            }
            // if (o.isLink) {
            //     vm.addFilter(n)
            // }

        }
        vm.delDimension = function(n) {
            var o = vm.dimensionsCfgMap[n];
            var inx = _.indexOf(vm.activeDimensionsNameArray, n)
            vm.restDimensionsNameArray.unshift(vm.activeDimensionsNameArray.splice(inx, 1)[0])
                // if (o.isLink) {
                //     vm.delFilter(n)
                // }
        }

        // //获取报表信息
        setTimeout(function() {
            if (RpIdSvc) {
                editRp(RpIdSvc)
            }else{
                console.log($rootScope.userInfo)
                 if($rootScope.userInfo.role!=='admin'){
                    vm['GID']=vm['GIDlist'][0]
                }
            }
        }, 0)

        // //获取报表信息
        function editRp(id) {
            Resource.getData('editReport', { id: id }).then(function(data) {
                var alldata = data
                var config = JSON.parse(data[0].config)
                console.log(config)
                vm.active = config.active;
                vm.metricsType = config.metricsType;
                vm.jobStart = new Date(config.jobStart)
                vm.jobEnd = new Date(config.jobEnd);
                vm.mailto = config.mailto;

                vm.period = config.repeats.type

                vm.dateRangeStart = new Date(config.dateRange.start)
                if (config.dateRange.end) {
                    vm.dateRangeEnd = new Date(config.dateRange.end)
                }

                var activeDimensionA = []


                config.dimensions.forEach(function(d) {
                    activeDimensionA.push(d.name)
                    if (vm.dimensionsCfgMap[d.name].isHaveData) {
                        vm['d' + d.name] = { id: d.value[0], text: d.text };
                    }
                })
                vm.activeDimensionsNameArray = activeDimensionA
                vm.restDimensionsNameArray = _.difference(vm.allDimensionsNameArray, vm.activeDimensionsNameArray);


                var activeFilterA = []
                config.filters.forEach(function(f) {
                    activeFilterA.push(f.name)
                });
                vm.activeFiltersNameArray = activeFilterA
                vm.fuckA = activeFilterA
                vm.restFiltersNameArray = _.difference(vm.allFiltersNameArray, vm.activeFiltersNameArray);

                setTimeout(function() {
                    config.filters.forEach(function(f) {
                        var type = vm.filtersCfgMap[f.name].type
                        var txt = f.text.split(',')
                        if (type == 'select') {
                            vm[f.name] = { id: f.value[0], text: f.text }
                        }
                        if (type == 'multipleselect') {

                            vm[f.name] = f.value.map(function(v, i) {
                                return { id: v, text: txt[i] }
                            })
                        }
                        if (type == 'doubleselect') {
                            vm[f.name + '1'] = f.valueStart
                            vm[f.name + '2'] = f.valueEnd
                        }
                    });

                }, 50)

            });

        }

        vm.test = function() {
            vm['GID'] = { id: '1000', text: 'demo' }
            vm.Network = { id: "7116", text: "7116" }
            vm.activeFiltersNameArray = vm.fuckA
        }


        $scope.$watchCollection(angular.bind(vm, function() {
            return [vm.aid, vm.cid, vm.gid]
        }), function(newVal) {
            if (newVal[0] && newVal[1] && newVal[2]) {
                Resource.getData('getCampaignStartData', {
                    gid: vm.gid,
                    aid: vm.aid,
                    cid: vm.cid
                }).then(function(data) {
                    var d = data[0]
                    if (data.length && !d.text) return false;
                    console.log(d.text, new Date(d.text))
                    vm.dateRangeStart = new Date(d.text)
                    vm.dateRangeFinish = d.end
                })
            }
        });


        $scope.$watchCollection(angular.bind(vm, function() {
            return [vm.jobStart, vm.jobEnd, vm.period]
        }), function(newVal) {
            var txt = {
                Daily: 'Every day',
                Weekly: 'Every Monday',
                Monthly: 'Day 1 each month'
            }
            var re = [
                txt[newVal[2]],
                ' from ',
                getFormateDate(newVal[0]),
                ', until ',
                getFormateDate(newVal[1])
            ].join('')
            vm.summary = re
        });

        vm.runReport = function() {
            if (!verify()) return false;
            vm.IsRunning = true;
            vm.actionInfo = '运行中。。。'

            $http({
                url: url.api,
                method: 'GET',
                params: {
                    op: 'getToken',
                    gid: vm.gid,
                    nid: vm.nid,
                    _: _.now()
                }
            }).then(function(re) {
                console.log('get token', re.data)
                cb(re.data)
            });

            function cb(re) {
                var postdata = getAllJson()
                postdata.token = re.token
                console.log('postdata', postdata)
            var _lang=$rootScope.isEn ? '&language=en' : '&language=zh'
                $http.post(url.api + '?op=runReport'+_lang, postdata)
                    .success(function(data) {
                        if (data.status == 200) {
                            vm.actionInfo = '运行成功'
                            console.log('run ok', data)
                            downloadFile(data.url)
                        }else{
                            vm.actionInfo = '运行失败'
                            // downloadFile(data.url)
                        }

                        setTimeout(function() {
                            vm.actionInfo = ''
                            vm.IsRunning = false
                        }, 3000)
                    }, function(er) {
                        console.log(er)
                        vm.IsRunning = false
                    })
            }


        };
        vm.saveReport = function() {
            if (!verify()) return false;
            vm.actionInfo = '保存中。。。'
            var _lang=$rootScope.isEn ? '&language=en' : '&language=zh'
            $http.post(url.api + '?op=saveReport'+_lang, getAllJson())
                .success(function(data) {
                    if (data.status == 200) {
                        vm.actionInfo = '保存成功'
                    }
                    setTimeout(function() {
                        vm.actionInfo = ''
                    }, 3000)
                })
        }


        function getAllJson() {

            var json = {
                summary: vm.summary,
                catalog: "AudienceProfile",
                status: vm.active ? "Schedule" : "Once",
                config: JSON.stringify(getConfig()),
                token: $rootScope.userInfo.token
            }

            json.name = [
                "AR_Report",
                vm.Network ? vm.Network.text : '',
                vm.Advertiser ? vm.Advertiser.text : '',
                vm.Campaign ? vm.Campaign.text : '',
                getNow(),
                vm.metricsType
            ].join('_');

            console.log(json)
            return json

        }

        function getConfig() {
            var config = {
                active: vm.active,
                reportName: "Audience Profile - " + vm.metricsType,
                jobStart: getFormateDate(vm.jobStart),
                jobEnd: getFormateDate(vm.jobEnd),
                mailto: vm.mailto,
                metricsType: vm.metricsType,
                dateRange: {
                    start: getFormateDate(vm.dateRangeStart),
                    end: getFormateDate(vm.dateRangeEnd),
                    finish: vm.dateRangeFinish
                },
                repeats: {
                    atWeekChoose: ["Sunday"],
                    daycheck: ['1'],
                    monthChoose: ["January"],
                    monthat: false,
                    monthday: true,
                    timeStart: '00:00',
                    type: vm.period,
                    weekChoose: ["Monday"],
                    weekOrderChoose: ['First']
                }
            };
            var filters = []
            vm.activeFiltersNameArray.forEach(function(n) {
                // if (!vm[n]) return;
                var o = {}
                o.name = n
                if (_.isArray(vm[n])) {
                    o.value = vm[n].map(function(a) {
                        return a.id
                    })
                    o.text = vm[n].map(function(a) {
                        return a.text
                    }).join(',')
                    o.editorType = 'combo'

                } else {
                    if (vm.filtersCfgMap[n].type == 'doubleselect') {
                        o.valueStart = vm[n + '1']
                        o.valueEnd = vm[n + '2']
                        o.text = vm[n + '1'] + ' - ' + vm[n + '2']
                        o.editorType = "numberComboRange"
                    } else {

                        o.value = [vm[n].id]
                        o.text = vm[n].text
                        o.editorType = 'combo'
                    }

                }
                filters.push(o)
            });

            config.filters = filters
            var dimensions = [];

            vm.activeDimensionsNameArray.forEach(function(n) {
                var o = {}
                o.name = n
                if (vm.dimensionsCfgMap[n].data) {
                    o.text = vm['d' + n].text
                    o.value = [vm['d' + n].id]
                }
                dimensions.push(o)
            });
            config.dimensions = dimensions
            console.log(config)
            return config;

        }

        function verify() {
            var fs = vm.activeFiltersNameArray;
            var fsmap = vm.filtersCfgMap
            if (!fs.length) {
                alert('未选择筛选条件')
                return false;
            }
            for (var k = 0; k < fs.length; k++) {
                var n = fs[k]
                var type = fsmap[n].type
                if (type === 'select') {
                    if (!vm[n]) {
                        alert('未选择 ' + fsmap[n].label)
                        return false;
                    }
                }
                if (type === 'multipleselect') {
                    if (!vm[n].length) {
                        alert('未选择 ' + fsmap[n].label)
                        return false;
                    }
                }
                if (type === 'doubleselect') {
                    if (!vm[n + '1'] || !vm[n + '2']) {
                        alert('未选择' + fsmap[n].label)
                        return false;
                    }
                }
            }

            // if(!vm.dateRangeEnd || !_.isDate(vm.dateRangeEnd)){
            //     alert('请选择报告结束周期')
            //     return false
            // }

            var re_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re_email.test(vm.mailto)) {
                alert('请输入正确的邮箱地址')
                return false;
            }
            return true;

        }


        function updateParams() {
            vm['gid'] = vm['GID'] ? vm['GID'].id : null
            vm['aid'] = vm['Advertiser'] ? vm['Advertiser'].id : null
            vm['cid'] = vm['Campaign'] ? vm['Campaign'].id : null
            vm['nid'] = vm['Network'] ? vm['Network'].id : null

            if (vm['Site']) {
                vm['sid'] = vm['Site'].map(function(k) {
                    return k.id
                }).join(',')
            }

        }

        var IS_FIRST_UPDATE=true;
        
        function updateFilterData(o) {
            if (o.op) {
                var params = {
                    op: o.op,
                    gid: vm['gid'],
                    aid: vm['aid'],
                    cid: vm['cid'],
                    nid: vm['nid'],
                    sid: vm['sid'],
                    token: $rootScope.userInfo.token,
                }
                $http({
                    url: url.api,
                    method: 'GET',
                    params: params
                }).then(function(res) {
                    vm[o.name + 'list'] = res.data.data;
                     if(o.name==='GID' && IS_FIRST_UPDATE){
                        IS_FIRST_UPDATE=false;
                        vm.GID=res.data.data[0]
                        setTimeout(function(){
                            vm.filterChange(vm.filtersCfgMap['Network'])
                        },0)
                    }
                    if (o.follower) {
                        o.follower.forEach(function(k) {
                            if (vm.filtersCfgMap[k].multiple) {
                                vm[k] = []
                            } else {
                                vm[k] = null
                            }
                        })
                    }
                })
            } else {
                vm[o.name + 'list'] = o.data
                if (o.type === 'doubleselect') {
                    console.log('doubleselect', o.data[0])
                    vm[o.name + '1'] = o.data[0][0]
                    vm[o.name + '2'] = o.data[1][0]
                }
            };

            if (o.follower) {
                o.follower.forEach(function(f) {
                    vm.filterChange(vm.filtersCfgMap[f])
                })
            }
        }



    }

    function downloadFile(url) {
        var a = document.createElement("a");
        a.href = url;
        // a.setAttribute('target',"_blank")
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    function getNow() {
        var d = new Date()
        var vYear = d.getFullYear()
        var vMon = d.getMonth() + 1
        var vDay = d.getDate()
        var h = d.getHours();
        var m = d.getMinutes();
        var se = d.getSeconds();
        if (vMon < 10) vMon = "0" + vMon;
        if (vDay < 10) vDay = "0" + vDay;
        if (h < 10) h = "0" + h;
        if (m < 10) m = "0" + m;
        if (se < 10) se = "0" + se;
        var ts = vMon + "-" + vDay + "-" + vYear + "_" + h + "_" + m + "_" + se;
        return ts;
    }


    function getFormateDate(o) {
        if (!o || !_.isDate(o)) return '';
        var m = o.getMonth() + 1
        var d = o.getDate()
        var y = o.getFullYear()
        m = m < 10 ? '0' + m : m
        d = d < 10 ? '0' + d : d
        return [m, d, y].join('/')
    }


})();
