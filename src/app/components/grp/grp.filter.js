(function() {
    'use strict';

    angular
        .module('adscan')
        .filter('tpFlt', tpFlt)
        .filter('propsFilter', propsFilter);

    function tpFlt() {
        return function(input) {
            if (typeof input === 'string') {
                return input
            } else {
                return input.text
            }
        }
    }


    function propsFilter() {
        return function(items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function(item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                out = items;
            }

            return out;
        };
    };

})();
