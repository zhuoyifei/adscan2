(function() {
    'use strict';

    angular
        .module('adscan')
        .controller('RcController', RcController);

    function RcController($scope, RpIdSvc, $rootScope, FiltersSvc, Resource, url, $http) {
        var vm = this;

        vm.title = "行业对标分析"
        vm.sid = []



        //筛选限制

        vm.allFiltersNameArray = [
            "Platform",
            "GID",
            "Network",
            "Advertiser",
            "Campaign",
            "Province",
            "City",
            "Site",
            "Placement",
            "Gender",
            "Age",
            "MPI"
        ]


        vm.filtersCfgMap = FiltersSvc.getMap(vm.allFiltersNameArray)
            //已选条件
        vm.activeFiltersNameArray = ['Platform', 'GID', 'Network', 'Advertiser', 'Campaign']
        vm.activeFiltersNameArray = []

        //待选条件
        vm.restFiltersNameArray = _.difference(vm.allFiltersNameArray, vm.activeFiltersNameArray)

        //初始化条件可选数据
        vm.allFiltersNameArray.forEach(function(v) {
            vm[v + 'list'] = []
        })

        //周期
        vm.range = [
            { id: '1', text: '前1个月' },
            { id: '3', text: '前3个月' },
            { id: '6', text: '前6个月' },
            { id: '12', text: '前12个月' }
        ]

        vm.rangeObj = {}
        vm.range.forEach(function(o) {
            vm.rangeObj[o.id] = o
        })

        vm.dateRange = { id: '1', text: '前1个月' }



        vm.addFilter = function(n) {
            var inx = _.indexOf(vm.restFiltersNameArray, n)
            var o = vm.filtersCfgMap[n]
            if (o.inverse) {
                var key = vm.activeFiltersNameArray.indexOf(o.inverse)
                if (key !== -1) {
                    vm.restFiltersNameArray.push(vm.activeFiltersNameArray.splice(key, 1)[0])
                }
            }
            vm.restFiltersNameArray.splice(inx, 1)
            vm.activeFiltersNameArray.push(n)

        }
        vm.delFilter = function(n) {
            var inx = _.indexOf(vm.activeFiltersNameArray, n)
            vm.restFiltersNameArray.unshift(vm.activeFiltersNameArray.splice(inx, 1)[0])
            _.isArray(vm[n]) ? vm[n] = [] : vm[n] = null;
            vm[n] = vm[n + '1'] = vm[n + '2'] = null;

        }
        vm.filterInit = function(o) {
            updateFilterData(o);
        }
        vm.filterChange = function(o) {
            updateParams();
            updateFilterData(o);
        }

        vm.removeItem = function(item, model, n) {
            for (var i = vm[n].length - 1; i >= 0; i--) {
                if (vm[n][i].id == item.id) {
                    vm[n].splice(i, 1)
                    break;
                }
            }
            vm.filterChange(vm.filtersCfgMap[n])
        }

        vm.runReport = function() {
            if (!verify()) return false;
            vm.actionInfo = '运行中。。。'

            $http({
                url: url.api,
                method: 'GET',
                params: {
                    op: 'getToken',
                    gid: vm.gid,
                    nid: vm.nid,
                    _: _.now()
                }
            }).then(function(re) {
                console.log(re.data)
                cb(re.data)
            });

            function cb(re) {
                var postdata = getAllJson()
                postdata.token = re.token
                console.log(postdata)
                var _lang = $rootScope.isEn ? '&language=en' : '&language=zh'
                $http.post(url.api + '?op=runReport' + _lang, postdata)
                    .success(function(data) {
                        if (data.status == 200) {
                            vm.actionInfo = '运行成功'
                            downloadFile(data.url)
                        } else {
                            vm.actionInfo = '运行失败'
                                // downloadFile(data.url)
                        }

                        setTimeout(function() {
                            vm.actionInfo = ''
                        }, 3000)
                    })
            }


        };
        vm.saveReport = function() {
            if (!verify()) return false;
            vm.actionInfo = '保存中。。。'
            var _lang = $rootScope.isEn ? '&language=en' : '&language=zh'
            $http.post(url.api + '?op=saveReport' + _lang, getAllJson())
                .success(function(data) {
                    if (data.status == 200) {
                        vm.actionInfo = '保存成功'
                    }
                    setTimeout(function() {
                        vm.actionInfo = ''
                    }, 3000)
                })
        }

        // //获取报表信息
        setTimeout(function() {
            if (RpIdSvc) {
                editRp(RpIdSvc)
            }
        }, 0)

        // //获取报表信息
        function editRp(id) {
            Resource.getData('editReport', { id: id }).then(function(data) {
                var config = JSON.parse(data[0].config)
                vm.mailto = config.mailto;


                var activeFilterA = []
                config.filters.forEach(function(f) {
                    activeFilterA.push(f.name)
                });
                vm.activeFiltersNameArray = activeFilterA
                vm.fuckA = activeFilterA
                vm.restFiltersNameArray = _.difference(vm.allFiltersNameArray, vm.activeFiltersNameArray);

                setTimeout(function() {
                    config.filters.forEach(function(f) {
                        var type = vm.filtersCfgMap[f.name].type
                        var txt = f.text.split(',')
                        if (type == 'select') {
                            vm[f.name] = { id: f.value[0], text: f.text }
                        }
                        if (type == 'multipleselect') {

                            vm[f.name] = f.value.map(function(v, i) {
                                return { id: v, text: txt[i] }
                            })
                        }
                        if (type == 'doubleselect') {
                            vm[f.name + '1'] = f.valueStart
                            vm[f.name + '2'] = f.valueEnd
                        }
                    });

                }, 500)

                console.log(config)

                vm.dateRange = vm.rangeObj[config.dateRange ? config.dateRange : '1']

            });

        }



        function getAllJson() {

            var json = {
                catalog: "ReachCurve",
                config: JSON.stringify(getConfig()),
                token: $rootScope.userInfo.token,
                mailto: vm.mailto
            }

            json.name = "RC_Report";

            [
                'Network',
                'Advertiser',
                'Placement',
                'City',
                'Gender',
                'Age',
                'MPI'
            ].forEach(function(k) {
                if (vm[k]) {
                    if (k == 'City') {
                        json.name += '_' + vm.City.map(function(v) {
                            return v.text }).join(',')
                    } else {
                        json.name += '_' + vm[k].text
                    }
                }
            });

            json.name += '_P';

            if (vm.dateRange) {
                json.name += vm.dateRange.id;
            }
            json.name += 'M_';
            json.name += getNow();
            console.log('new name', json.name)
                // json.name = [
                //     "RC_Report",
                //     vm.Network ? vm.Network.text : '',
                //     vm.Advertiser ? vm.Advertiser.text : '',
                //     vm.Placement ? vm.Placement.text : '',
                //     vm.City ? vm.City.text : '',
                //     vm.Gender ? vm.Gender.text : '',
                //     vm.Age ? vm.Age.text : '',
                //     vm.MPI ? vm.MPI.text : '',
                //     "P",
                //     vm.dateRange ? vm.dateRange.id : '',
                //     "M",
                //     getNow()
                // ].join('_');

            console.log(json)
            return json

        }

        function getConfig() {
            var config = {
                active: vm.active,
                reportName: "Reach Curve - " + vm.metricsType,
                dateRange: vm.dateRange.id,
                mailto: vm.mailto
            };
            var filters = []
            vm.activeFiltersNameArray.forEach(function(n) {
                // if (!vm[n]) return;
                var o = {}
                o.name = n
                if (_.isArray(vm[n])) {
                    o.value = vm[n].map(function(a) {
                        return a.id
                    })
                    o.text = vm[n].map(function(a) {
                        return a.text
                    }).join(',')

                } else {
                    if (vm.filtersCfgMap[n].type == 'doubleselect') {
                        o.valueStart = vm[n + '1']
                        o.valueEnd = vm[n + '2']
                        o.text = vm[n + '1'] + ' - ' + vm[n + '2']
                    } else {
                        o.value = [vm[n].id]
                        o.text = vm[n].text
                    }

                }
                filters.push(o)
            });
            config.filters = filters
            return config;
        }

        function verify() {
            var fs = vm.activeFiltersNameArray;
            var fsmap = vm.filtersCfgMap
            console.log('fsmap', fsmap)
            if (!fs.length) {
                alert('未选择筛选条件')
                return false;
            }
            for (var k = 0; k < fs.length; k++) {
                var n = fs[k]
                var type = fsmap[n].type
                if (type === 'select') {
                    if (!vm[n]) {
                        alert('未选择 ' + fsmap[n].label)
                        return false;
                    }
                }
                if (type === 'multipleselect') {
                    if (!vm[n].length) {
                        alert('未选择 ' + fsmap[n].label)
                        return false;
                    }
                }
                if (type === 'doubleselect') {
                    if (!vm[n + '1'] || !vm[n + '2']) {
                        alert('未选择' + fsmap[n].label)
                        return false;
                    }
                }
            }

            // if(!vm.dateRangeEnd || !_.isDate(vm.dateRangeEnd)){
            //     alert('请选择报告结束周期')
            //     return false
            // }

            var re_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re_email.test(vm.mailto)) {
                alert('请输入正确的邮箱地址')
                return false;
            }
            return true;

        }


        function updateParams() {
            vm['gid'] = vm['GID'] ? vm['GID'].id : null
            vm['aid'] = vm['Advertiser'] ? vm['Advertiser'].id : null
            vm['cid'] = vm['Campaign'] ? vm['Campaign'].id : null
            vm['nid'] = vm['Network'] ? vm['Network'].id : null

            if (vm['Site']) {
                vm['sid'] = vm['Site'].map(function(k) {
                    return k.id
                }).join(',')
            }

        }

        function updateFilterData(o) {
            if (o.op) {
                var params = {
                    op: o.op,
                    gid: vm['gid'],
                    aid: vm['aid'],
                    cid: vm['cid'],
                    nid: vm['nid'],
                    sid: vm['sid'],
                    token: $rootScope.userInfo.token,
                }
                $http({
                    url: url.api,
                    method: 'GET',
                    params: params
                }).then(function(res) {
                    vm[o.name + 'list'] = res.data.data;

                    if (o.follower) {
                        o.follower.forEach(function(k) {
                            if (vm.filtersCfgMap[k].multiple) {
                                vm[k] = []
                            } else {
                                vm[k] = null
                            }
                        })
                    }
                })
            } else {
                vm[o.name + 'list'] = o.data

                if (o.type === 'doubleselect') {
                    vm[o.name + '1'] = o.data[0][0]
                    vm[o.name + '2'] = o.data[1][0]
                }
            };

            if (o.follower) {
                o.follower.forEach(function(f) {
                    vm.filterChange(vm.filtersCfgMap[f])
                })
            }
        }


    }

    function downloadFile(url) {
        var a = document.createElement("a");
        a.href = url;
        // a.setAttribute('target',"_blank")
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    function getNow() {
        var d = new Date()
        var vYear = d.getFullYear()
        var vMon = d.getMonth() + 1
        var vDay = d.getDate()
        var h = d.getHours();
        var m = d.getMinutes();
        var se = d.getSeconds();
        if (vMon < 10) vMon = "0" + vMon;
        if (vDay < 10) vDay = "0" + vDay;
        if (h < 10) h = "0" + h;
        if (m < 10) m = "0" + m;
        if (se < 10) se = "0" + se;
        var ts = vMon + "-" + vDay + "-" + vYear + "_" + h + "_" + m + "_" + se;
        return ts;
    }


    function getFormateDate(o) {
        if (!o || !_.isDate(o)) return '';
        var m = o.getMonth() + 1
        var d = o.getDate()
        var y = o.getFullYear()
        m = m < 10 ? '0' + m : m
        d = d < 10 ? '0' + d : d
        return [m, d, y].join('/')
    }


})();
