(function() {
    'use strict';

    angular
        .module('adscan')
        .controller('ApaController', ApaController);

    /** @ngInject */
    function ApaController($http, $cookies,$rootScope, url, Resource, demoData) {
        var vm = this
        var userInfo = $cookies.getObject('userInfo');
        var isAdminDemo = false
        var i = 0

        reset()


        vm.render = function(aid, cid) {
            vm.aid = aid
            vm.cid = cid
            reset()

            if (/demo/i.test(userInfo.username)) {
                demoShow()
                return false;
            }
            if (userInfo.username == 'admin' && !isAdminDemo) {
                
                demoShow()
                isAdminDemo = true
                return false;
            }

            detail()
            trend()
            effect()

            planAndCoast()
            target()
            rank()
            siteeffect()
        }

        function demoShow() {
            vm.detail = demoData.apa_detail.rows
            vm.trend = 1
            var re = handleTrendData(demoData.apa_trend.data);
            hcTrend(re,$rootScope);
            vm.effect = _.chunk(handleEffectNumber(demoData.apa_effect.rows), 4);


            var hexcolor = [
                ["#0d345a", '#39bdf9'],
                ["#aa2429", '#ff4e4e']
            ];
            ['progress_plan', 'progress_cost'].forEach(function(k, i) {
                var info = demoData['apa_' + k]
                if (!info) return;
                vm[k] = true
                halfPie({
                    id: "#hc-" + k.substring(9),
                    backColor: hexcolor[i][0],
                    frontColor: hexcolor[i][1],
                    startText: info.from,
                    endText: info.to,
                    per: info.value / 100,
                    current: info.cost,
                })

            });

            //target
            ['progress_target_cpm', 'progress_target_ctr'].forEach(function(k) {
                vm[k] = demoData[k]
            });

            //rank

            ['topsite', 'topregion'].forEach(function(k) {
                vm[k] = handRank(demoData['apa_' + k].data)
            });

            //siteeffect
             vm.siteeffect = handleEffect(demoData['apa_siteeffect'].rows.slice(2))


        } //demoShow end

        function reset() {
            vm.detail = []
            vm.trend = 0
            vm.effect = []
            vm.progress_cost = false
            vm.progress_plan = false
            vm.target_cpm = vm.target_ctr = null
            vm.topsite = []
            vm.topregion = []
        }

        function detail() {
            Resource.getKpiData('apa_detail', { cid: vm.cid.id, aid: vm.aid.id }).then(function(data) {
                vm.detail = data.rows
            }, function() {
                vm.detail = []
                console.log(er)
            })

        }

        function trend() {
            Resource.getKpiData('apa_trend', { cid: vm.cid.id, aid: vm.aid.id }).then(function(data) {
                vm.trend = 1
                var re = handleTrendData(data.data)
                hcTrend(re,$rootScope)
            }, function() {
                vm.trend = 0
                console.log(er)
            })
        }

        function effect() {
            Resource.getKpiData('apa_effect', { cid: vm.cid.id, aid: vm.aid.id }).then(function(data) {
                vm.effect = _.chunk(handleEffectNumber(data.rows), 4)
            }, function() {
                vm.effect = []
            })
        }

        function planAndCoast() {
            var hexcolor = [
                ["#0d345a", '#39bdf9'],
                ["#aa2429", '#ff4e4e']
            ];
            ['progress_plan', 'progress_cost'].forEach(function(k, i) {

                Resource.getKpiData('apa_' + k, { cid: vm.cid.id, aid: vm.aid.id }).then(function(data) {
                    var info = data.data[0]

                    console.log(k,info)
                    if (!info) return;
                    vm[k] = true
                    halfPie({
                        id: "#hc-" + k.substring(9),
                        backColor: hexcolor[i][0],
                        frontColor: hexcolor[i][1],
                        startText: info.from,
                        endText: info.to,
                        per: info.value / 100,
                        current: info.cost,
                    })
                }, function(er) {
                    vm[k] = false
                    console.log(er)
                });

            })

        }

        function target() {
            ['progress_target_cpm', 'progress_target_ctr'].forEach(function(k) {
                Resource
                    .getKpiData('apa_' + k, { cid: vm.cid.id, aid: vm.aid.id })
                    .then(function(data) {
                        console.log(k,data)
                        vm[k] = data.data[0]
                        if(!data.data[0]) vm[k]=null
                    }, function(er) {
                        vm[k] = null
                        console.log(er)
                    });
            })

        }

        function rank() {

            ['topsite', 'topregion'].forEach(function(k) {
                Resource
                    .getKpiData('apa_' + k, { cid: vm.cid.id, aid: vm.aid.id })
                    .then(function(data) {
                        console.log(k,data.data)
                        vm[k] = handRank(data.data)
                    }, function(er) {
                        vm[k] = []
                        console.log(k, er)
                    })
            })
        }


        function siteeffect() {
            Resource
                .getKpiData('apa_siteeffect', { cid: vm.cid.id, aid: vm.aid.id })
                .then(function(data) {
                    vm.siteeffect = handleEffect(data.rows.slice(2))
                }, function(er) {
                    vm.siteeffect = []
                    console.log('apa_siteeffect',er)
                })
        }
    }


    function handRank(ar) {
        var nums = ar.map(function(v) {
            return v.count
        })
        var max = Math.max.apply(Math, nums)

        var addper = ar.map(function(o) {
            o.per = o.count * 100 / max + '%'
            return o
        })
        return _.sortBy(addper, function(o) {
            return o.count
        })
    }

    function handleEffect(ar) {
        var aro = {}
        for (var i = 0; i < ar.length; i++) {
            var notail = _.trim(ar[i].siteName.replace('Total', ''))
            aro[notail] = notail in aro ? aro[notail] : []
            aro[notail].push(ar[i])
        }
        return aro
    }


    function hcTrend(opt,rootScope) {
        Highcharts.chart("hc-trend", {
            chart: {
                zoomType: 'xy'
            },
            credits: {
                enabled: 0
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: [{
                categories: opt.categories
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}',
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text:rootScope.isEn ? "Impression" : "曝光量",
                    style: {
                        color: '#0071d0'
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: rootScope.isEn ? "CTR" : '点击率',
                    style: {
                        color: '#d00000'
                    }
                },
                labels: {
                    format: '{value} %',
                    style: {
                        color: '#4572A7'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: 1
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                y: -10,
                floating: true,
            },
            series: [{
                name: rootScope.isEn ? "Impression" : "曝光量",
                color: '#0071d0',
                type: 'column',
                yAxis: 0,
                data: opt.impression,
            }, {
                name: rootScope.isEn ? "Goal-impression" : '目标曝光量',
                color: 'green',
                type: 'spline',
                yAxis: 0,
                data: opt.goal_impression
            }, {
                name: rootScope.isEn ? "CTR" : '点击率',
                color: '#d00000',
                type: 'spline',
                yAxis: 1,
                data: opt.ctr,
                tooltip: {
                    valueSuffix: ' %'
                }
            }]
        })
    }

    function handleTrendData(ar) {
        var re = {}
        re.categories = []
        re.impression = []
        re.goal_impression = []
        re.ctr = []

        ar.forEach(function(o) {
            re.ctr.push(o.ctr)
            re.categories.push(o.ad_date)
            re.impression.push(o.impression)
            re.goal_impression.push(o.goal_impression)
        })

        return re
    }

    function handleEffectNumber(rows) {
        if (rows.length !== 8) return [];
        var re = []
        _.chunk(rows, 4).forEach(function(ar) {
            _.keys(ar[0]).forEach(function(k) {
                re.push({
                    name: ar[2][k],
                    value: ar[1][k],
                    per: ar[3][k]
                })
            })
        })
        return re
    }

    function halfPie(opt) {

        // $(opt.id).html("<canvas></canvas>")
        var parent = document.getElementById(opt.id.slice(1))
        parent.innerHTML = "<canvas></canvas>"

        // var canvas = $(opt.id).find('canvas')[0];
        var canvas = parent.getElementsByTagName('canvas')[0];
        var context = canvas.getContext('2d');
        canvas.width *= 3
        canvas.height *= 3
        var x = canvas.width / 2;
        var y = canvas.height - 60;
        var radius = 300;
        var deg = Math.PI / 180;

        context.beginPath();
        context.arc(x, y, radius, 0, 180 * deg, 1);
        context.lineWidth = 80;
        context.strokeStyle = opt.backColor;
        context.stroke();

        context.beginPath();
        context.arc(x, y, radius, 180 * deg, Math.min(2, 1 + opt.per) * 180 * deg);
        context.strokeStyle = opt.frontColor;
        context.stroke();

        context.font = "30px microsoft yahei";
        var start = opt.startText
        context.fillText(start, x - radius - getTextWidth(start) / 2, 420);
        var end = opt.endText
        context.fillText(end, x + radius - getTextWidth(end) / 2, 420);

        context.font = "75px Arial";
        var per = (opt.per * 100 + '').substring(0, 4) + '%'
        context.fillText(per, x - getTextWidth(per) / 2, y - 20);

        context.font = "40px Arial";
        context.fillStyle = opt.frontColor
        var current = opt.current
        context.fillText(current, x - getTextWidth(current) / 2, y - 150);

        function getTextWidth(txt) {
            return context.measureText(txt).width
        }
    }

})();
