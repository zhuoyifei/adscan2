(function() {
    'use strict';

    angular
        .module('adscan')
        .controller('RcaController', RcaController);


    function RcaController($http, url, $cookies, demoData, Resource) {
        var vm = this
        var userInfo = $cookies.getObject('userInfo');
        var isAdminDemo = false
        vm.tableData = []


        vm.render = function(aid, cid) {
            vm.aid = aid
            vm.cid = cid
            vm.tableData = []


            if (/demo/i.test(userInfo.username)) {
                demoShow()
                return false;
            }
            if (userInfo.username == 'admin' && !isAdminDemo) {

                demoShow()
                isAdminDemo = true
                return false;
            }

            function demoShow() {
                var ar = demoData['apa_siteeffect_rca'].rows.slice(2)
                vm.tableData = handleData(ar)
            }

            Resource
                .getKpiData('apa_siteeffect_rca', {
                    cid: vm.cid.id,
                    aid: vm.aid.id
                })
                .then(function(data){
                    var ar = data.data.slice(2)
                    vm.tableData = handleData(ar)
                }, function(er) {
                    vm.tableData = []
                    console.log(er)
                })
        }
    }


    function handleData(ar) {
        var aro = {}
        for (var i = 0; i < ar.length; i++) {
            var notail = _.trim(ar[i].siteName.replace('Total', ''))
            aro[notail] = notail in aro ? aro[notail] : []
            aro[notail].push(ar[i])
        }
        return aro
    }

})();
