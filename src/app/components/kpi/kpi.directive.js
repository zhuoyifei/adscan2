(function() {
    'use strict';

    angular
        .module('adscan')
        .directive('aidcid', aidcid);

    /** @ngInject */
    function aidcid($cookies) {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/kpi/tmpl/aidcid.html',
            replace: true,
            link: linkFunc,
            scope: {
                renderFn: "&",
            },
            controller: AidCidCtrl,
            controllerAs: 'vm'
        };

        return directive

        function linkFunc(scope) {
            scope.run = function(a, c) {
                scope.renderFn({ aid: a, cid: c })
            }
        }

        function AidCidCtrl($scope, Resource, $rootScope, aidSvc, cidSvc) {
            var vm = this
            var token = $rootScope.userInfo.token
            var isFirst = true

            $scope.isEn=$rootScope.isEn
            var isAdminDemo = $rootScope.userInfo.role==='admin' || $rootScope.userInfo.username=='demo';
            Resource.getData('listKPIaid', { token: token }).then(function(data) {
                if(!data.length){
                    data.push({ id: "demo", text: "demo" })
                }
                vm.listAid=data
                if (isAdminDemo) {
                    vm.aid = { id: "demo", text: "demo" }
                } else {
                    vm.aid = data[0]
                }
                vm.listCid=[]
                vm.getCid(isAdminDemo)
            })


            vm.getCid = function(isAdminDemo) {
                Resource.getData('listKPIcid', { aid: vm.aid.id }).then(function(data) {
                    vm.listCid = data
                    vm.cid = data[0] || { id: "demo", text: "demo" }


                    var username = $cookies.getObject('userInfo').username;
                    if ('once') {
                        $scope.run(vm.aid, vm.cid)
                    }
                })
            }

        }


    }

})();
