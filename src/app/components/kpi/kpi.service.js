(function() {
    'use strict';

    angular
        .module('adscan')
        .service('demoData', demoData)

    function demoData($rootScope) {
        var zh = {
            //apa start
            apa_detail: { "total": 7, "rows": [{ "itemid": "广告名称", "itemvalue": "demo" }, { "itemid": "投放状态", "itemvalue": "已结束" }, { "itemid": "广告预算", "itemvalue": "￥已结束" }, { "itemid": "广告进度", "itemvalue": "每日¥500.00" }, { "itemid": "广告目标", "itemvalue": "CPM,¥1.50 and CTR,0.150%" }, { "itemid": "频率上线", "itemvalue": "一周5次不超过5次曝光" }, { "itemid": "广告排期", "itemvalue": "2015/03/24 to 2015/07/06" }] },
            apa_trend: { "data": [{ "ad_date": "20150929", "impression": 1, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151005", "impression": 9, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151012", "impression": 10123538, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151019", "impression": 39822040, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151026", "impression": 108490755, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151102", "impression": 77396677, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151109", "impression": 34643844, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151116", "impression": 54037598, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151123", "impression": 17602329, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151130", "impression": 278, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151207", "impression": 51, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151214", "impression": 50, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151221", "impression": 33, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151228", "impression": 23, "ctr": 0, "goal_impression": 0 }] },
            apa_effect: {
                "total": 8,
                "rows": [{ "a": "", "b": "", "c": "", "d": "" },
                    { "a": "15.1M", "b": "27.8K", "c": "0.18%", "d": "￥31.3K" },
                    { "a": "曝光次数", "b": "点击次数", "c": "广告点击率", "d": "花费" },
                    { "a": "+7%", "b": "+9%", "c": "+1%", "d": "-2%" },
                    { "a": "", "b": "", "c": "", "d": "" },
                    { "a": "￥2.10", "b": "￥1.13", "c": "9.7M", "d": "1.60" },
                    { "a": "千人成本", "b": "点击付费", "c": "曝光人数", "d": "平均曝光次数" },
                    { "a": "-5%", "b": "-10%", "c": "+4%", "d": "+2%" }
                ]
            },
            progress_target_cpm: { "name": "累积的CPM(￥)", "target": 16034267, "current": 10034267 },
            progress_target_ctr: { "name": "累积的CTR(￥)", "target": 80, "current": 20 },
            apa_progress_plan: { "name": "进度", "from": "2015/09/24", "to": "2016/01/01", "cost": "105 Days", "left": "0 Days", "value": 90 },
            apa_progress_cost: { "name": "花费", "from": "￥0", "to": "￥200", "cost": "￥100", "left": "￥100", "value": 50 },
            apa_topsite: { "data": [{ "name": "funshion.com", "count": 4952287 }, { "name": "demo.com", "count": 4052287 }, { "name": "Baofeng.com", "count": 3257447 }, { "name": "iqiyi.com", "count": 1903818 }, { "name": "youku.com", "count": 1400156 }] },
            apa_topregion: { "data": [{ "name": "上海", "count": 4959273 }, { "name": "北京", "count": 4351720 }, { "name": "江苏_其他", "count": 774399 }, { "name": "天津", "count": 502310 }, { "name": "忻州", "count": 59795 }] },
            apa_siteeffect: {
                "total": 16,
                "rows": [
                    { "siteName": "网站", "adLocation": "广告位", "pv": "广告曝光", "uv": "", "click": "广告点击", "uc": "", "pvp1": "点击率", "pvp2": "目标达成率", "clickp": "", "cpv": "KPI", "cpm": "", "cpc": "", "cpa": "" },
                    { "siteName": "", "adLocation": "", "pv": "PV", "uv": "UV", "click": "Click", "uc": "UC", "pvp1": "(PV)", "pvp2": "PV %", "clickp": "Click %", "cpv": "CPV", "cpm": "CPM", "cpc": "CPC", "cpa": "CPA" },
                    { "siteName": "网站 A", "adLocation": "26.pre", "pv": "1029955", "uv": "235060", "click": "20989", "uc": "14671", "pvp1": "2.00%", "pvp2": "2.00%", "clickp": "162.50%", "cpv": "￥0.6", "cpm": "￥4.9", "cpc": "￥0.1", "cpa": "￥0.6" },
                    { "siteName": "网站 A", "adLocation": "6.Tips", "pv": "41298713", "uv": "32555275", "click": "3753618", "uc": "3049741", "pvp1": "9.10%", "pvp2": "9.10%", "clickp": "312.80%", "cpv": "￥0.6", "cpm": "￥4.9", "cpc": "￥0.1", "cpa": "￥0.6" },
                    { "siteName": "网站 A", "adLocation": "7.play", "pv": "39210594", "uv": "17705918", "click": "311600", "uc": "274312", "pvp1": "0.80%", "pvp2": "0.80%", "clickp": "82.00%", "cpv": "￥0.6", "cpm": "￥4.9", "cpc": "￥0.1", "cpa": "￥0.6" },
                    { "siteName": "网站 A Total", "adLocation": "", "pv": "81539262", "uv": "50496253", "click": "4086207", "uc": "3338724", "pvp1": "5.01%", "pvp2": "239.79%", "clickp": "258.62%", "cpv": "", "cpm": "", "cpc": "", "cpa": "" },
                    { "siteName": "网站 B", "adLocation": "17.全网_i", "pv": "12996479", "uv": "5604302", "click": "1670207", "uc": "1060549", "pvp1": "12.90%", "pvp2": "12.90%", "clickp": "417.60%", "cpv": "￥1.1", "cpm": "￥29.6", "cpc": "￥0.2", "cpa": "￥0.7" },
                    { "siteName": "网站 B", "adLocation": "18.全网_I", "pv": "502128", "uv": "100989", "click": "3535", "uc": "3066", "pvp1": "0.70%", "pvp2": "0.70%", "clickp": "417.60%", "cpv": "￥1.1", "cpm": "￥29.6", "cpc": "￥0.2", "cpa": "￥0.7" },
                    { "siteName": "网站 B Total", "adLocation": "", "pv": "13498607", "uv": "5705291", "click": "1673742", "uc": "1063615", "pvp1": "12.40%", "pvp2": "97.50%", "clickp": "417.60%", "cpv": "", "cpm": "", "cpc": "", "cpa": "" },
                    { "siteName": "网站 C", "adLocation": "3.Home", "pv": "4904714", "uv": "3970841", "click": "7994", "uc": "7485", "pvp1": "0.20%", "pvp2": "0.20%", "clickp": "75.30%", "cpv": "￥32.9", "cpm": "￥6.4", "cpc": "￥11.3", "cpa": "￥30.7" },
                    { "siteName": "网站 C", "adLocation": "4.Tran", "pv": "33360000", "uv": "1920541", "click": "9798", "uc": "9230", "pvp1": "0.60%", "pvp2": "0.58%", "clickp": "54.40%", "cpv": "￥32.9", "cpm": "￥6.4", "cpc": "￥11.3", "cpa": "￥30.7" },
                    { "siteName": "网站 C", "adLocation": "5.Gardens", "pv": "24670000", "uv": "14670506", "click": "17690", "uc": "15187", "pvp1": "0.10%", "pvp2": "0.10%", "clickp": "168.50%", "cpv": "￥32.9", "cpm": "￥6.4", "cpc": "￥11.3", "cpa": "￥30.7" },
                    { "siteName": "网站 C Total", "adLocation": "", "pv": "62934714", "uv": "3970841", "click": "35482", "uc": "31902", "pvp1": "0.06%", "pvp2": "65.90%", "clickp": "90.69%", "cpv": "", "cpm": "", "cpc": "", "cpa": "" },
                    { "siteName": "网站 D", "adLocation": "12.Drama", "pv": "11655445", "uv": "8747966", "click": "428672", "uc": "346830", "pvp1": "3.70%", "pvp2": "3.70%", "clickp": "191.10%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "网站 D", "adLocation": "13.Drama", "pv": "8633068", "uv": "6350367", "click": "81050", "uc": "69249", "pvp1": "0.90%", "pvp2": "0.90%", "clickp": "121.60%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "网站 D", "adLocation": "14.Search", "pv": "105741396", "uv": "2227017", "click": "352152", "uc": "78945", "pvp1": "0.30%", "pvp2": "0.30%", "clickp": "106.70%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "网站 D", "adLocation": "15.Movie", "pv": "25416244", "uv": "5684091", "click": "2568", "uc": "2324", "pvp1": "0.15%", "pvp2": "0.28%", "clickp": "64.20%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "网站 D", "adLocation": "16.Drama", "pv": "20259782", "uv": "2610013", "click": "3123", "uc": "2857", "pvp1": "0.39%", "pvp2": "0.19%", "clickp": "62.50%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "网站 D", "adLocation": "28.Home", "pv": "13255962", "uv": "8882554", "click": "72259", "uc": "1844", "pvp1": "0.50%", "pvp2": "0.50%", "clickp": "67.70%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "网站 D Total", "adLocation": "", "pv": "184961897", "uv": "15098333", "click": "939824", "uc": "421260", "pvp1": "0.51%", "pvp2": "112.42%", "clickp": "149.18%", "cpv": "", "cpm": "", "cpc": "", "cpa": "" }
                ]
            },
            //apa end

            //aaa start
            aaa_gender_a: { "data": [{ "type": "广告曝光用户", "name": "男", "value": 64.2 }, { "type": "广告曝光用户", "name": "女", "value": 35.8 }, { "type": "全体网民", "name": "男", "value": 71.9 }, { "type": "全体网民", "name": "女", "value": 28.1 }] },
            aaa_gender_b: { "data": [{ "type": "广告点击用户", "name": "男", "value": 64.2 }, { "type": "广告点击用户", "name": "女", "value": 35.8 }, { "type": "全体网民", "name": "男", "value": 71.9 }, { "type": "全体网民", "name": "女", "value": 28.1 }] },
            aaa_income_a: { "data": [{ "type": "广告曝光用户", "name": "<2000", "value": 4.1 }, { "type": "广告曝光用户", "name": "2000-3000", "value": 19.8 }, { "type": "广告曝光用户", "name": "3000-5000", "value": 30.4 }, { "type": "广告曝光用户", "name": "5000-8000", "value": 25.3 }, { "type": "广告曝光用户", "name": "8000-10000", "value": 12.3 }, { "type": "广告曝光用户", "name": "10000-15000", "value": 5.4 }, { "type": "广告曝光用户", "name": ">15000", "value": 2.7 }, { "type": "全体网民", "name": "<2000", "value": 4.1 }, { "type": "全体网民", "name": "2000-3000", "value": 14 }, { "type": "全体网民", "name": "3000-5000", "value": 24.8 }, { "type": "全体网民", "name": "5000-8000", "value": 29.9 }, { "type": "全体网民", "name": "8000-10000", "value": 18.1 }, { "type": "全体网民", "name": "10000-15000", "value": 6.1 }, { "type": "全体网民", "name": ">15000", "value": 3 }] },
            aaa_income_b: { "data": [{ "type": "广告点击用户", "name": "<2000", "value": 4.1 }, { "type": "广告点击用户", "name": "2000-3000", "value": 19.8 }, { "type": "广告点击用户", "name": "3000-5000", "value": 30.4 }, { "type": "广告点击用户", "name": "5000-8000", "value": 25.3 }, { "type": "广告点击用户", "name": "8000-10000", "value": 12.3 }, { "type": "广告点击用户", "name": "10000-15000", "value": 5.4 }, { "type": "广告点击用户", "name": ">15000", "value": 2.7 }, { "type": "全体网民", "name": "<2000", "value": 4.1 }, { "type": "全体网民", "name": "2000-3000", "value": 14 }, { "type": "全体网民", "name": "3000-5000", "value": 24.8 }, { "type": "全体网民", "name": "5000-8000", "value": 29.9 }, { "type": "全体网民", "name": "8000-10000", "value": 18.1 }, { "type": "全体网民", "name": "10000-15000", "value": 6.1 }, { "type": "全体网民", "name": ">15000", "value": 3 }] },
            aaa_age_a: { "data": [{ "type": "广告曝光用户", "name": "<19", "value": 10 }, { "type": "广告曝光用户", "name": "20-24", "value": 14.7 }, { "type": "广告曝光用户", "name": "25-29", "value": 23.7 }, { "type": "广告曝光用户", "name": "30-34", "value": 17.5 }, { "type": "广告曝光用户", "name": "35-39", "value": 13 }, { "type": "广告曝光用户", "name": "40-44", "value": 8.1 }, { "type": "广告曝光用户", "name": "45-49", "value": 7.7 }, { "type": "广告曝光用户", "name": ">50", "value": 5.3 }, { "type": "全体网民", "name": "<19", "value": 13.5 }, { "type": "全体网民", "name": "20-24", "value": 15.7 }, { "type": "全体网民", "name": "25-29", "value": 22.2 }, { "type": "全体网民", "name": "30-34", "value": 16.6 }, { "type": "全体网民", "name": "35-39", "value": 18 }, { "type": "全体网民", "name": "40-44", "value": 4.2 }, { "type": "全体网民", "name": "45-49", "value": 5 }, { "type": "全体网民", "name": ">50", "value": 4.7 }] },
            aaa_age_b: { "data": [{ "type": "广告点击用户", "name": "<19", "value": 10 }, { "type": "广告点击用户", "name": "20-24", "value": 14.7 }, { "type": "广告点击用户", "name": "25-29", "value": 23.7 }, { "type": "广告点击用户", "name": "30-34", "value": 17.5 }, { "type": "广告点击用户", "name": "35-39", "value": 13 }, { "type": "广告点击用户", "name": "40-44", "value": 8.1 }, { "type": "广告点击用户", "name": "45-49", "value": 7.7 }, { "type": "广告点击用户", "name": ">50", "value": 5.3 }, { "type": "全体网民", "name": "<19", "value": 13.5 }, { "type": "全体网民", "name": "20-24", "value": 15.7 }, { "type": "全体网民", "name": "25-29", "value": 22.2 }, { "type": "全体网民", "name": "30-34", "value": 16.6 }, { "type": "全体网民", "name": "35-39", "value": 18 }, { "type": "全体网民", "name": "40-44", "value": 4.2 }, { "type": "全体网民", "name": "45-49", "value": 5 }, { "type": "全体网民", "name": ">50", "value": 4.7 }] },
            aaa_edu_a: { "data": [{ "type": "广告曝光用户", "name": "高中及以下", "value": 4.1 }, { "type": "广告曝光用户", "name": "大学专科", "value": 19.8 }, { "type": "广告曝光用户", "name": "大学本科", "value": 40.4 }, { "type": "广告曝光用户", "name": "硕士研究生", "value": 25.3 }, { "type": "广告曝光用户", "name": "博士研究生", "value": 10.3 }, { "type": "全体网民", "name": "高中及以下", "value": 4.1 }, { "type": "全体网民", "name": "大学专科", "value": 19.8 }, { "type": "全体网民", "name": "大学本科", "value": 40.4 }, { "type": "全体网民", "name": "硕士研究生", "value": 29.3 }, { "type": "全体网民", "name": "博士研究生", "value": 6.3 }] },
            aaa_edu_b: { "data": [{ "type": "广告点击用户", "name": "高中及以下", "value": 4.1 }, { "type": "广告点击用户", "name": "大学专科", "value": 19.8 }, { "type": "广告点击用户", "name": "大学本科", "value": 40.4 }, { "type": "广告点击用户", "name": "硕士研究生", "value": 25.3 }, { "type": "广告点击用户", "name": "博士研究生", "value": 10.3 }, { "type": "全体网民", "name": "高中及以下", "value": 4.1 }, { "type": "全体网民", "name": "大学专科", "value": 19.8 }, { "type": "全体网民", "name": "大学本科", "value": 40.4 }, { "type": "全体网民", "name": "硕士研究生", "value": 29.3 }, { "type": "全体网民", "name": "博士研究生", "value": 6.3 }] },
            //aaa end

            //rca start
            apa_siteeffect_rca: { "total": 16, "rows": [{ "siteName": "网站", "adLocation": "广告位", "bounceRate": "到达页", "reach": "", "uv": "", "ru": "", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站", "adLocation": "广告位", "bounceRate": "跳出率", "reach": "到达率", "uv": "独立用户数", "ru": "注册数量（用户）", "price": "价格", "priceAvg": "平均价格", "regCtb": "注册贡献度" }, { "siteName": "网站 A", "adLocation": "26.pre", "bounceRate": "62.5%", "reach": "37.5%", "uv": "257467", "ru": "7876", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 A", "adLocation": "6.Tips", "bounceRate": "84.4%", "reach": "15.6%", "uv": "107106", "ru": "587386", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 A", "adLocation": "7.play", "bounceRate": "64.5%", "reach": "35.5%", "uv": "243735", "ru": "110967", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 A Total", "adLocation": "", "bounceRate": "70.47%", "reach": "29.53%", "uv": "608308", "ru": "706229", "price": "", "priceAvg": "", "regCtb": "34.29%" }, { "siteName": "网站 B", "adLocation": "17.全网_i", "bounceRate": "64.1%", "reach": "35.9%", "uv": "134314", "ru": "601322", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 B", "adLocation": "18.全网_I", "bounceRate": "53.9%", "reach": "46.1%", "uv": "172476", "ru": "1642", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 B Total", "adLocation": "", "bounceRate": "59.00%", "reach": "41.00%", "uv": "306790", "ru": "602964", "price": "", "priceAvg": "", "regCtb": "80.58%" }, { "siteName": "网站 C", "adLocation": "3.Home", "bounceRate": "51.2%", "reach": "48.8%", "uv": "5928", "ru": "4261", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 C", "adLocation": "4.Tran", "bounceRate": "71.1%", "reach": "28.9%", "uv": "3510", "ru": "3001", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 C", "adLocation": "5.Gardens", "bounceRate": "68.6%", "reach": "31.4%", "uv": "3814", "ru": "5777", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 C Total", "adLocation": "", "bounceRate": "63.63%", "reach": "36.37%", "uv": "13252", "ru": "13039", "price": "", "priceAvg": "", "regCtb": "35.78%" }, { "siteName": "网站 D", "adLocation": "12.Drama", "bounceRate": "92.7%", "reach": "7.3%", "uv": "3928", "ru": "31382", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 D", "adLocation": "13.Drama", "bounceRate": "73.3%", "reach": "26.7%", "uv": "14365", "ru": "21744", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 D", "adLocation": "14.Search", "bounceRate": "100%", "reach": "0%", "uv": "0", "ru": "400", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 D", "adLocation": "15.Movie", "bounceRate": "62.5%", "reach": "37.5%", "uv": "20176", "ru": "987", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 D", "adLocation": "16.Drama", "bounceRate": "60.7%", "reach": "39.3%", "uv": "21144", "ru": "1335", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 D", "adLocation": "28.Home", "bounceRate": "100%", "reach": "0%", "uv": "0", "ru": "0", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "网站 D Total", "adLocation": "", "bounceRate": "81.53%", "reach": "18.47%", "uv": "59613", "ru": "55848", "price": "", "priceAvg": "", "regCtb": "17.3%" }] },
            //rca end

            //aoa start

            aoa_uvreach: {
                "data": [{ "name": "网站 C", "value": "3.8" }, { "name": "网站 B", "value": "7.1" }, { "name": "网站 D", "value": "17.8" }, { "name": "网站 E", "value": "18.0" }, { "name": "网站 F", "value": "11.6" }, { "name": "网站 A", "value": "41.7" }]
            },
            aoa_duplication: {
                "data": [{ "name": "网站 E", "value": "16.8" }, { "name": "网站 C", "value": "14.7" }, { "name": "网站 B", "value": "16.0" }, { "name": "网站 D", "value": "16.4" }, { "name": "网站 F", "value": "14.7" }, { "name": "网站 A", "value": "8.0" }]
            },
            aoa_duplication_table1: {
                "data": [{ "a": "网站 D", "b": "网站 D", "c": "12879363" }, { "a": "网站 D", "b": "网站 E", "c": "1020931" }, { "a": "网站 D", "b": "网站 C", "c": "76520" }, { "a": "网站 D", "b": "网站 F", "c": "382374" }, { "a": "网站 D", "b": "网站 B", "c": "231365" }, { "a": "网站 D", "b": "网站 A", "c": "697502" }, { "a": "网站 E", "b": "网站 E", "c": "13061065" }, { "a": "网站 E", "b": "网站 C", "c": "59307" }, { "a": "网站 E", "b": "网站 F", "c": "407953" }, { "a": "网站 E", "b": "网站 B", "c": "208846" }, { "a": "网站 E", "b": "网站 A", "c": "798655" }, { "a": "网站 C", "b": "网站 C", "c": "2691112" }, { "a": "网站 C", "b": "网站 F", "c": "48189" }, { "a": "网站 C", "b": "网站 B", "c": "38896" }, { "a": "网站 C", "b": "网站 A", "c": "214315" }, { "a": "网站 F", "b": "网站 F", "c": "8200684" }, { "a": "网站 F", "b": "网站 B", "c": "163445" }, { "a": "网站 F", "b": "网站 A", "c": "414153" }, { "a": "网站 B", "b": "网站 B", "c": "5098765" }, { "a": "网站 B", "b": "网站 A", "c": "312437" }, { "a": "网站 A", "b": "网站 A", "c": "27331659" }]
            },
            aoa_duplication_table2: {
                "data": [{ "a": "网站 D", "b": "网站 D", "c": "100.0" }, { "a": "网站 D", "b": "网站 E", "c": "7.9" }, { "a": "网站 D", "b": "网站 C", "c": "0.6" }, { "a": "网站 D", "b": "网站 F", "c": "3.0" }, { "a": "网站 D", "b": "网站 B", "c": "1.8" }, { "a": "网站 D", "b": "网站 A", "c": "5.4" }, { "a": "网站 E", "b": "网站 D", "c": "7.8" }, { "a": "网站 E", "b": "网站 E", "c": "100.0" }, { "a": "网站 E", "b": "网站 C", "c": "0.5" }, { "a": "网站 E", "b": "网站 F", "c": "3.1" }, { "a": "网站 E", "b": "网站 B", "c": "1.6" }, { "a": "网站 E", "b": "网站 A", "c": "6.1" }, { "a": "网站 C", "b": "网站 D", "c": "2.8" }, { "a": "网站 C", "b": "网站 E", "c": "2.8" }, { "a": "网站 C", "b": "网站 C", "c": "100.0" }, { "a": "网站 C", "b": "网站 F", "c": "1.8" }, { "a": "网站 C", "b": "网站 B", "c": "1.4" }, { "a": "网站 C", "b": "网站 A", "c": "8.0" }, { "a": "网站 F", "b": "网站 D", "c": "4.7" }, { "a": "网站 F", "b": "网站 E", "c": "5.0" }, { "a": "网站 F", "b": "网站 C", "c": "0.6" }, { "a": "网站 F", "b": "网站 F", "c": "100.0" }, { "a": "网站 F", "b": "网站 B", "c": "2.0" }, { "a": "网站 F", "b": "网站 A", "c": "5.1" }, { "a": "网站 B", "b": "网站 D", "c": "4.5" }, { "a": "网站 B", "b": "网站 E", "c": "4.1" }, { "a": "网站 B", "b": "网站 C", "c": "0.8" }, { "a": "网站 B", "b": "网站 F", "c": "3.2" }, { "a": "网站 B", "b": "网站 B", "c": "100.0" }, { "a": "网站 B", "b": "网站 A", "c": "6.1" }, { "a": "网站 A", "b": "网站 D", "c": "2.6" }, { "a": "网站 A", "b": "网站 E", "c": "2.9" }, { "a": "网站 A", "b": "网站 C", "c": "0.8" }, { "a": "网站 A", "b": "网站 F", "c": "1.5" }, { "a": "网站 A", "b": "网站 B", "c": "1.1" }, { "a": "网站 A", "b": "网站 A", "c": "100.0" }, { "a": "网站 D", "b": "网站 D", "c": "100.0" }, { "a": "网站 D", "b": "网站 E", "c": "7.8" }, { "a": "网站 D", "b": "网站 C", "c": "2.8" }, { "a": "网站 D", "b": "网站 F", "c": "4.7" }, { "a": "网站 D", "b": "网站 B", "c": "4.5" }, { "a": "网站 D", "b": "网站 A", "c": "2.6" }, { "a": "网站 E", "b": "网站 D", "c": "7.9" }, { "a": "网站 E", "b": "网站 E", "c": "100.0" }, { "a": "网站 E", "b": "网站 C", "c": "2.8" }, { "a": "网站 E", "b": "网站 F", "c": "5.0" }, { "a": "网站 E", "b": "网站 B", "c": "4.1" }, { "a": "网站 E", "b": "网站 A", "c": "2.9" }, { "a": "网站 C", "b": "网站 D", "c": "0.6" }, { "a": "网站 C", "b": "网站 E", "c": "0.5" }, { "a": "网站 C", "b": "网站 C", "c": "100.0" }, { "a": "网站 C", "b": "网站 F", "c": "0.6" }, { "a": "网站 C", "b": "网站 B", "c": "0.8" }, { "a": "网站 C", "b": "网站 A", "c": "0.8" }, { "a": "网站 F", "b": "网站 D", "c": "3.0" }, { "a": "网站 F", "b": "网站 E", "c": "3.1" }, { "a": "网站 F", "b": "网站 C", "c": "1.8" }, { "a": "网站 F", "b": "网站 F", "c": "100.0" }, { "a": "网站 F", "b": "网站 B", "c": "3.2" }, { "a": "网站 F", "b": "网站 A", "c": "1.5" }, { "a": "网站 B", "b": "网站 D", "c": "1.8" }, { "a": "网站 B", "b": "网站 E", "c": "1.6" }, { "a": "网站 B", "b": "网站 C", "c": "1.4" }, { "a": "网站 B", "b": "网站 F", "c": "2.0" }, { "a": "网站 B", "b": "网站 B", "c": "100.0" }, { "a": "网站 B", "b": "网站 A", "c": "1.1" }, { "a": "网站 A", "b": "网站 D", "c": "5.4" }, { "a": "网站 A", "b": "网站 E", "c": "6.1" }, { "a": "网站 A", "b": "网站 C", "c": "8.0" }, { "a": "网站 A", "b": "网站 F", "c": "5.1" }, { "a": "网站 A", "b": "网站 B", "c": "6.1" }, { "a": "网站 A", "b": "网站 A", "c": "100.0" }]
            },
            aoa_duplication_table3: {
                "data": [{ "website": "网站", "pv": "PV", "uv": "UV Reach", "euv": "Exclusive UV Reach", "euvp": "Exclusive UV Reach%", "dup": "Duplication%" }, { "website": "网站 D", "pv": "20288513", "uv": "12879363", "euv": "10767418", "euvp": "17.8%", "dup": "16.4%" }, { "website": "网站 E", "pv": "16244659", "uv": "13061065", "euv": "10860559", "euvp": "18.0%", "dup": "16.8%" }, { "website": "网站 C", "pv": "4904714", "uv": "2691112", "euv": "2294858", "euvp": "3.8%", "dup": "14.7%" }, { "website": "网站 F", "pv": "14471608", "uv": "8200684", "euv": "6993508", "euvp": "11.6%", "dup": "14.7%" }, { "website": "网站 B", "pv": "13498607", "uv": "5098765", "euv": "4282783", "euvp": "7.1%", "dup": "16.0%" }, { "website": "网站 A", "pv": "77539266", "uv": "27331659", "euv": "25132345", "euvp": "41.7%", "dup": "8.0%" }]
            }

            //aoa end

        }
        var en = {
            apa_detail: {
                "total": 7,
                "rows": [{
                    "itemid": "Name",
                    "itemvalue": "demo"
                }, {
                    "itemid": "Status",
                    "itemvalue": "Finished"
                }, {
                    "itemid": "Budget",
                    "itemvalue": "￥500000"
                }, {
                    "itemid": "Pacing",
                    "itemvalue": "¥500.00 /d"
                }, {
                    "itemid": "Goal",
                    "itemvalue": "CPM,¥1.50 and CTR,0.150%"
                }, {
                    "itemid": "Frequency Cap",
                    "itemvalue": "--"
                }, {
                    "itemid": "Schedule",
                    "itemvalue": "2015/03/24 to 2015/07/06"
                }]
            },
            apa_trend: {
                "data": [{
                    "ad_date": "20150929",
                    "impression": 1,
                    "ctr": 0,
                    "goal_impression": 0
                }, {
                    "ad_date": "20151005",
                    "impression": 9,
                    "ctr": 0,
                    "goal_impression": 0
                }, {
                    "ad_date": "20151012",
                    "impression": 10123538,
                    "ctr": 0,
                    "goal_impression": 0
                }, {
                    "ad_date": "20151019",
                    "impression": 39822040,
                    "ctr": 0,
                    "goal_impression": 0
                }, {
                    "ad_date": "20151026",
                    "impression": 108490755,
                    "ctr": 0,
                    "goal_impression": 0
                }, { "ad_date": "20151102", "impression": 77396677, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151109", "impression": 34643844, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151116", "impression": 54037598, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151123", "impression": 17602329, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151130", "impression": 278, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151207", "impression": 51, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151214", "impression": 50, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151221", "impression": 33, "ctr": 0, "goal_impression": 0 }, { "ad_date": "20151228", "impression": 23, "ctr": 0, "goal_impression": 0 }]
            },
            apa_effect: {
                "total": 8,
                "rows": [{ "a": "", "b": "", "c": "", "d": "" },
                    { "a": "15.1M", "b": "27.8K", "c": "0.18%", "d": "￥31.3K" },
                    { "a": "Impression", "b": "Click", "c": "CTR", "d": "Cost" },
                    { "a": "+7%", "b": "+9%", "c": "+1%", "d": "-2%" },
                    { "a": "", "b": "", "c": "", "d": "" },
                    { "a": "￥2.10", "b": "￥1.13", "c": "9.7M", "d": "1.60" },
                    { "a": "CPM", "b": "CPC", "c": "UV-Impression", "d": "Avg Frequency" },
                    { "a": "-5%", "b": "-10%", "c": "+4%", "d": "+2%" }
                ]
            },
            progress_target_cpm: { "name": "Cumulative CPM(￥)", "target": 16034267, "current": 10034267 },
            progress_target_ctr: { "name": "Cumulative CTR(￥)", "target": 80, "current": 20 },
            apa_progress_plan: { "name": "进度", "from": "2015/09/24", "to": "2016/01/01", "cost": "105 Days", "left": "0 Days", "value": 90 },
            apa_progress_cost: { "name": "花费", "from": "￥0", "to": "￥200", "cost": "￥100", "left": "￥100", "value": 50 },
            apa_topsite: { "data": [{ "name": "funshion.com", "count": 4952287 }, { "name": "demo.com", "count": 4052287 }, { "name": "Baofeng.com", "count": 3257447 }, { "name": "iqiyi.com", "count": 1903818 }, { "name": "youku.com", "count": 1400156 }] },
            apa_topregion: { "data": [{ "name": "上海", "count": 4959273 }, { "name": "北京", "count": 4351720 }, { "name": "江苏_其他", "count": 774399 }, { "name": "天津", "count": 502310 }, { "name": "忻州", "count": 59795 }] },
            apa_siteeffect: {
                "total": 16,
                "rows": [
                    { "siteName": "Site", "adLocation": "广告位", "pv": "广告曝光", "uv": "", "click": "广告点击", "uc": "", "pvp1": "点击率", "pvp2": "目标达成率", "clickp": "", "cpv": "KPI", "cpm": "", "cpc": "", "cpa": "" },
                    { "siteName": "", "adLocation": "", "pv": "PV", "uv": "UV", "click": "Click", "uc": "UC", "pvp1": "(PV)", "pvp2": "PV %", "clickp": "Click %", "cpv": "CPV", "cpm": "CPM", "cpc": "CPC", "cpa": "CPA" },
                    { "siteName": "Site A", "adLocation": "26.pre", "pv": "1029955", "uv": "235060", "click": "20989", "uc": "14671", "pvp1": "2.00%", "pvp2": "2.00%", "clickp": "162.50%", "cpv": "￥0.6", "cpm": "￥4.9", "cpc": "￥0.1", "cpa": "￥0.6" },
                    { "siteName": "Site A", "adLocation": "6.Tips", "pv": "41298713", "uv": "32555275", "click": "3753618", "uc": "3049741", "pvp1": "9.10%", "pvp2": "9.10%", "clickp": "312.80%", "cpv": "￥0.6", "cpm": "￥4.9", "cpc": "￥0.1", "cpa": "￥0.6" },
                    { "siteName": "Site A", "adLocation": "7.play", "pv": "39210594", "uv": "17705918", "click": "311600", "uc": "274312", "pvp1": "0.80%", "pvp2": "0.80%", "clickp": "82.00%", "cpv": "￥0.6", "cpm": "￥4.9", "cpc": "￥0.1", "cpa": "￥0.6" },
                    { "siteName": "Site A Total", "adLocation": "", "pv": "81539262", "uv": "50496253", "click": "4086207", "uc": "3338724", "pvp1": "5.01%", "pvp2": "239.79%", "clickp": "258.62%", "cpv": "", "cpm": "", "cpc": "", "cpa": "" },
                    { "siteName": "Site B", "adLocation": "17.全网_i", "pv": "12996479", "uv": "5604302", "click": "1670207", "uc": "1060549", "pvp1": "12.90%", "pvp2": "12.90%", "clickp": "417.60%", "cpv": "￥1.1", "cpm": "￥29.6", "cpc": "￥0.2", "cpa": "￥0.7" },
                    { "siteName": "Site B", "adLocation": "18.全网_I", "pv": "502128", "uv": "100989", "click": "3535", "uc": "3066", "pvp1": "0.70%", "pvp2": "0.70%", "clickp": "417.60%", "cpv": "￥1.1", "cpm": "￥29.6", "cpc": "￥0.2", "cpa": "￥0.7" },
                    { "siteName": "Site B Total", "adLocation": "", "pv": "13498607", "uv": "5705291", "click": "1673742", "uc": "1063615", "pvp1": "12.40%", "pvp2": "97.50%", "clickp": "417.60%", "cpv": "", "cpm": "", "cpc": "", "cpa": "" },
                    { "siteName": "Site C", "adLocation": "3.Home", "pv": "4904714", "uv": "3970841", "click": "7994", "uc": "7485", "pvp1": "0.20%", "pvp2": "0.20%", "clickp": "75.30%", "cpv": "￥32.9", "cpm": "￥6.4", "cpc": "￥11.3", "cpa": "￥30.7" },
                    { "siteName": "Site C", "adLocation": "4.Tran", "pv": "33360000", "uv": "1920541", "click": "9798", "uc": "9230", "pvp1": "0.60%", "pvp2": "0.58%", "clickp": "54.40%", "cpv": "￥32.9", "cpm": "￥6.4", "cpc": "￥11.3", "cpa": "￥30.7" },
                    { "siteName": "Site C", "adLocation": "5.Gardens", "pv": "24670000", "uv": "14670506", "click": "17690", "uc": "15187", "pvp1": "0.10%", "pvp2": "0.10%", "clickp": "168.50%", "cpv": "￥32.9", "cpm": "￥6.4", "cpc": "￥11.3", "cpa": "￥30.7" },
                    { "siteName": "Site C Total", "adLocation": "", "pv": "62934714", "uv": "3970841", "click": "35482", "uc": "31902", "pvp1": "0.06%", "pvp2": "65.90%", "clickp": "90.69%", "cpv": "", "cpm": "", "cpc": "", "cpa": "" },
                    { "siteName": "Site D", "adLocation": "12.Drama", "pv": "11655445", "uv": "8747966", "click": "428672", "uc": "346830", "pvp1": "3.70%", "pvp2": "3.70%", "clickp": "191.10%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "Site D", "adLocation": "13.Drama", "pv": "8633068", "uv": "6350367", "click": "81050", "uc": "69249", "pvp1": "0.90%", "pvp2": "0.90%", "clickp": "121.60%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "Site D", "adLocation": "14.Search", "pv": "105741396", "uv": "2227017", "click": "352152", "uc": "78945", "pvp1": "0.30%", "pvp2": "0.30%", "clickp": "106.70%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "Site D", "adLocation": "15.Movie", "pv": "25416244", "uv": "5684091", "click": "2568", "uc": "2324", "pvp1": "0.15%", "pvp2": "0.28%", "clickp": "64.20%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "Site D", "adLocation": "16.Drama", "pv": "20259782", "uv": "2610013", "click": "3123", "uc": "2857", "pvp1": "0.39%", "pvp2": "0.19%", "clickp": "62.50%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "Site D", "adLocation": "28.Home", "pv": "13255962", "uv": "8882554", "click": "72259", "uc": "1844", "pvp1": "0.50%", "pvp2": "0.50%", "clickp": "67.70%", "cpv": "￥9.3", "cpm": "￥2.7", "cpc": "￥0.5", "cpa": "￥9" },
                    { "siteName": "Site D Total", "adLocation": "", "pv": "184961897", "uv": "15098333", "click": "939824", "uc": "421260", "pvp1": "0.51%", "pvp2": "112.42%", "clickp": "149.18%", "cpv": "", "cpm": "", "cpc": "", "cpa": "" }
                ]
            },
            //apa end

            //aaa start
            aaa_gender_a: {
                "data": [{
                    "type": "Exposed Audience",
                    "name": "Male",
                    "value": 64.2
                }, {
                    "type": "Exposed Audience",
                    "name": "Male",
                    "value": 35.8
                }, {
                    "type": "Internet Population",
                    "name": "Female",
                    "value": 71.9
                }, {
                    "type": "Internet Population",
                    "name": "Female",
                    "value": 28.1
                }]
            },
            aaa_gender_b: {
                "data": [{
                        "type": "Clicked Audience",
                        "name": "Male",
                        "value": 64.2
                    },
                    { "type": "Clicked Audience", "name": "Female", "value": 35.8 },
                    { "type": "Internet Population", "name": "Male", "value": 71.9 },
                    { "type": "Internet Population", "name": "Female", "value": 28.1 }
                ]
            },
            aaa_income_a: {
                "data": [{
                        "type": "Exposed Audience",
                        "name": "<2000",
                        "value": 4.1
                    },
                    { "type": "Exposed Audience", "name": "2000-3000", "value": 19.8 },
                    { "type": "Exposed Audience", "name": "3000-5000", "value": 30.4 },
                    { "type": "Exposed Audience", "name": "5000-8000", "value": 25.3 },
                    { "type": "Exposed Audience", "name": "8000-10000", "value": 12.3 },
                    { "type": "Exposed Audience", "name": "10000-15000", "value": 5.4 },
                    { "type": "Exposed Audience", "name": ">15000", "value": 2.7 },
                    { "type": "Internet Population", "name": "<2000", "value": 4.1 },
                    { "type": "Internet Population", "name": "2000-3000", "value": 14 },
                    { "type": "Internet Population", "name": "3000-5000", "value": 24.8 },
                    { "type": "Internet Population", "name": "5000-8000", "value": 29.9 },
                    { "type": "Internet Population", "name": "8000-10000", "value": 18.1 },
                    { "type": "Internet Population", "name": "10000-15000", "value": 6.1 },
                    { "type": "Internet Population", "name": ">15000", "value": 3 }
                ]
            },
            aaa_income_b: {
                "data": [{
                        "type": "Clicked Audience",
                        "name": "<2000",
                        "value": 4.1
                    },
                    { "type": "Clicked Audience", "name": "2000-3000", "value": 19.8 },
                    { "type": "Clicked Audience", "name": "3000-5000", "value": 30.4 },
                    { "type": "Clicked Audience", "name": "5000-8000", "value": 25.3 },
                    { "type": "Clicked Audience", "name": "8000-10000", "value": 12.3 },
                    { "type": "Clicked Audience", "name": "10000-15000", "value": 5.4 },
                    { "type": "Clicked Audience", "name": ">15000", "value": 2.7 },
                    { "type": "Internet Population", "name": "<2000", "value": 4.1 },
                    { "type": "Internet Population", "name": "2000-3000", "value": 14 },
                    { "type": "Internet Population", "name": "3000-5000", "value": 24.8 },
                    { "type": "Internet Population", "name": "5000-8000", "value": 29.9 },
                    { "type": "Internet Population", "name": "8000-10000", "value": 18.1 },
                    { "type": "Internet Population", "name": "10000-15000", "value": 6.1 },
                    { "type": "Internet Population", "name": ">15000", "value": 3 }
                ]
            },
            aaa_age_a: {
                "data": [{
                        "type": "Exposed Audience",
                        "name": "<19",
                        "value": 10
                    }, { "type": "Exposed Audience", "name": "20-24", "value": 14.7 },
                    { "type": "Exposed Audience", "name": "25-29", "value": 23.7 },
                    { "type": "Exposed Audience", "name": "30-34", "value": 17.5 },
                    { "type": "Exposed Audience", "name": "35-39", "value": 13 },
                    { "type": "Exposed Audience", "name": "40-44", "value": 8.1 },
                    { "type": "Exposed Audience", "name": "45-49", "value": 7.7 },
                    { "type": "Exposed Audience", "name": ">50", "value": 5.3 },
                    { "type": "Internet Population", "name": "<19", "value": 13.5 },
                    { "type": "Internet Population", "name": "20-24", "value": 15.7 },
                    { "type": "Internet Population", "name": "25-29", "value": 22.2 },
                    { "type": "Internet Population", "name": "30-34", "value": 16.6 },
                    { "type": "Internet Population", "name": "35-39", "value": 18 },
                    { "type": "Internet Population", "name": "40-44", "value": 4.2 },
                    { "type": "Internet Population", "name": "45-49", "value": 5 },
                    { "type": "Internet Population", "name": ">50", "value": 4.7 }
                ]
            },
            aaa_age_b: {
                "data": [{
                        "type": "Clicked Audience",
                        "name": "<19",
                        "value": 10
                    }, { "type": "Clicked Audience", "name": "20-24", "value": 14.7 },
                    { "type": "Clicked Audience", "name": "25-29", "value": 23.7 },
                    { "type": "Clicked Audience", "name": "30-34", "value": 17.5 },
                    { "type": "Clicked Audience", "name": "35-39", "value": 13 },
                    { "type": "Clicked Audience", "name": "40-44", "value": 8.1 },
                    { "type": "Clicked Audience", "name": "45-49", "value": 7.7 },
                    { "type": "Clicked Audience", "name": ">50", "value": 5.3 },
                    { "type": "Internet Population", "name": "<19", "value": 13.5 },
                    { "type": "Internet Population", "name": "20-24", "value": 15.7 },
                    { "type": "Internet Population", "name": "25-29", "value": 22.2 },
                    { "type": "Internet Population", "name": "30-34", "value": 16.6 },
                    { "type": "Internet Population", "name": "35-39", "value": 18 },
                    { "type": "Internet Population", "name": "40-44", "value": 4.2 },
                    { "type": "Internet Population", "name": "45-49", "value": 5 },
                    { "type": "Internet Population", "name": ">50", "value": 4.7 }
                ]
            },
            aaa_edu_a: {
                "data": [{
                    "type": "Exposed Audience",
                    "name": "High School",
                    "value": 4.1
                }, { "type": "Exposed Audience", "name": "College Diploma", "value": 19.8 }, { "type": "Exposed Audience", "name": "University Diploma", "value": 40.4 }, { "type": "Exposed Audience", "name": "Master Degree", "value": 25.3 }, { "type": "Exposed Audience", "name": "Doctor Degree", "value": 10.3 }, { "type": "Internet Population", "name": "High School", "value": 4.1 }, { "type": "Internet Population", "name": "College Diploma", "value": 19.8 }, { "type": "Internet Population", "name": "University Diploma", "value": 40.4 }, { "type": "Internet Population", "name": "Master Degree", "value": 29.3 }, { "type": "Internet Population", "name": "Doctor Degree", "value": 6.3 }]
            },
            aaa_edu_b: {
                "data": [{
                    "type": "Clicked Audience",
                    "name": "High School",
                    "value": 4.1
                }, { "type": "Clicked Audience", "name": "College Diploma", "value": 19.8 }, { "type": "Clicked Audience", "name": "University Diploma", "value": 40.4 }, { "type": "Clicked Audience", "name": "Master Degree", "value": 25.3 }, { "type": "Clicked Audience", "name": "Doctor Degree", "value": 10.3 }, { "type": "Internet Population", "name": "High School", "value": 4.1 }, { "type": "Internet Population", "name": "College Diploma", "value": 19.8 }, { "type": "Internet Population", "name": "University Diploma", "value": 40.4 }, { "type": "Internet Population", "name": "Master Degree", "value": 29.3 }, { "type": "Internet Population", "name": "Doctor Degree", "value": 6.3 }]
            },
            //aaa end

            //rca start
            apa_siteeffect_rca: { "total": 16, "rows": [{ "siteName": "Site", "adLocation": "广告位", "bounceRate": "到达页", "reach": "", "uv": "", "ru": "", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site", "adLocation": "广告位", "bounceRate": "跳出率", "reach": "到达率", "uv": "独立用户数", "ru": "注册数量（用户）", "price": "价格", "priceAvg": "平均价格", "regCtb": "注册贡献度" }, { "siteName": "Site A", "adLocation": "26.pre", "bounceRate": "62.5%", "reach": "37.5%", "uv": "257467", "ru": "7876", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site A", "adLocation": "6.Tips", "bounceRate": "84.4%", "reach": "15.6%", "uv": "107106", "ru": "587386", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site A", "adLocation": "7.play", "bounceRate": "64.5%", "reach": "35.5%", "uv": "243735", "ru": "110967", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site A Total", "adLocation": "", "bounceRate": "70.47%", "reach": "29.53%", "uv": "608308", "ru": "706229", "price": "", "priceAvg": "", "regCtb": "34.29%" }, { "siteName": "Site B", "adLocation": "17.全网_i", "bounceRate": "64.1%", "reach": "35.9%", "uv": "134314", "ru": "601322", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site B", "adLocation": "18.全网_I", "bounceRate": "53.9%", "reach": "46.1%", "uv": "172476", "ru": "1642", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site B Total", "adLocation": "", "bounceRate": "59.00%", "reach": "41.00%", "uv": "306790", "ru": "602964", "price": "", "priceAvg": "", "regCtb": "80.58%" }, { "siteName": "Site C", "adLocation": "3.Home", "bounceRate": "51.2%", "reach": "48.8%", "uv": "5928", "ru": "4261", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site C", "adLocation": "4.Tran", "bounceRate": "71.1%", "reach": "28.9%", "uv": "3510", "ru": "3001", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site C", "adLocation": "5.Gardens", "bounceRate": "68.6%", "reach": "31.4%", "uv": "3814", "ru": "5777", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site C Total", "adLocation": "", "bounceRate": "63.63%", "reach": "36.37%", "uv": "13252", "ru": "13039", "price": "", "priceAvg": "", "regCtb": "35.78%" }, { "siteName": "Site D", "adLocation": "12.Drama", "bounceRate": "92.7%", "reach": "7.3%", "uv": "3928", "ru": "31382", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site D", "adLocation": "13.Drama", "bounceRate": "73.3%", "reach": "26.7%", "uv": "14365", "ru": "21744", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site D", "adLocation": "14.Search", "bounceRate": "100%", "reach": "0%", "uv": "0", "ru": "400", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site D", "adLocation": "15.Movie", "bounceRate": "62.5%", "reach": "37.5%", "uv": "20176", "ru": "987", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site D", "adLocation": "16.Drama", "bounceRate": "60.7%", "reach": "39.3%", "uv": "21144", "ru": "1335", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site D", "adLocation": "28.Home", "bounceRate": "100%", "reach": "0%", "uv": "0", "ru": "0", "price": "", "priceAvg": "", "regCtb": "" }, { "siteName": "Site D Total", "adLocation": "", "bounceRate": "81.53%", "reach": "18.47%", "uv": "59613", "ru": "55848", "price": "", "priceAvg": "", "regCtb": "17.3%" }] },
            //rca end

            //aoa start

            aoa_uvreach: {
                "data": [{ "name": "Site C", "value": "3.8" }, { "name": "Site B", "value": "7.1" }, { "name": "Site D", "value": "17.8" }, { "name": "Site E", "value": "18.0" }, { "name": "Site F", "value": "11.6" }, { "name": "Site A", "value": "41.7" }]
            },
            aoa_duplication: {
                "data": [{ "name": "Site E", "value": "16.8" }, { "name": "Site C", "value": "14.7" }, { "name": "Site B", "value": "16.0" }, { "name": "Site D", "value": "16.4" }, { "name": "Site F", "value": "14.7" }, { "name": "Site A", "value": "8.0" }]
            },
            aoa_duplication_table1: {
                "data": [{ "a": "Site D", "b": "Site D", "c": "12879363" }, { "a": "Site D", "b": "Site E", "c": "1020931" }, { "a": "Site D", "b": "Site C", "c": "76520" }, { "a": "Site D", "b": "Site F", "c": "382374" }, { "a": "Site D", "b": "Site B", "c": "231365" }, { "a": "Site D", "b": "Site A", "c": "697502" }, { "a": "Site E", "b": "Site E", "c": "13061065" }, { "a": "Site E", "b": "Site C", "c": "59307" }, { "a": "Site E", "b": "Site F", "c": "407953" }, { "a": "Site E", "b": "Site B", "c": "208846" }, { "a": "Site E", "b": "Site A", "c": "798655" }, { "a": "Site C", "b": "Site C", "c": "2691112" }, { "a": "Site C", "b": "Site F", "c": "48189" }, { "a": "Site C", "b": "Site B", "c": "38896" }, { "a": "Site C", "b": "Site A", "c": "214315" }, { "a": "Site F", "b": "Site F", "c": "8200684" }, { "a": "Site F", "b": "Site B", "c": "163445" }, { "a": "Site F", "b": "Site A", "c": "414153" }, { "a": "Site B", "b": "Site B", "c": "5098765" }, { "a": "Site B", "b": "Site A", "c": "312437" }, { "a": "Site A", "b": "Site A", "c": "27331659" }]
            },
            aoa_duplication_table2: {
                "data": [{ "a": "Site D", "b": "Site D", "c": "100.0" }, { "a": "Site D", "b": "Site E", "c": "7.9" }, { "a": "Site D", "b": "Site C", "c": "0.6" }, { "a": "Site D", "b": "Site F", "c": "3.0" }, { "a": "Site D", "b": "Site B", "c": "1.8" }, { "a": "Site D", "b": "Site A", "c": "5.4" }, { "a": "Site E", "b": "Site D", "c": "7.8" }, { "a": "Site E", "b": "Site E", "c": "100.0" }, { "a": "Site E", "b": "Site C", "c": "0.5" }, { "a": "Site E", "b": "Site F", "c": "3.1" }, { "a": "Site E", "b": "Site B", "c": "1.6" }, { "a": "Site E", "b": "Site A", "c": "6.1" }, { "a": "Site C", "b": "Site D", "c": "2.8" }, { "a": "Site C", "b": "Site E", "c": "2.8" }, { "a": "Site C", "b": "Site C", "c": "100.0" }, { "a": "Site C", "b": "Site F", "c": "1.8" }, { "a": "Site C", "b": "Site B", "c": "1.4" }, { "a": "Site C", "b": "Site A", "c": "8.0" }, { "a": "Site F", "b": "Site D", "c": "4.7" }, { "a": "Site F", "b": "Site E", "c": "5.0" }, { "a": "Site F", "b": "Site C", "c": "0.6" }, { "a": "Site F", "b": "Site F", "c": "100.0" }, { "a": "Site F", "b": "Site B", "c": "2.0" }, { "a": "Site F", "b": "Site A", "c": "5.1" }, { "a": "Site B", "b": "Site D", "c": "4.5" }, { "a": "Site B", "b": "Site E", "c": "4.1" }, { "a": "Site B", "b": "Site C", "c": "0.8" }, { "a": "Site B", "b": "Site F", "c": "3.2" }, { "a": "Site B", "b": "Site B", "c": "100.0" }, { "a": "Site B", "b": "Site A", "c": "6.1" }, { "a": "Site A", "b": "Site D", "c": "2.6" }, { "a": "Site A", "b": "Site E", "c": "2.9" }, { "a": "Site A", "b": "Site C", "c": "0.8" }, { "a": "Site A", "b": "Site F", "c": "1.5" }, { "a": "Site A", "b": "Site B", "c": "1.1" }, { "a": "Site A", "b": "Site A", "c": "100.0" }, { "a": "Site D", "b": "Site D", "c": "100.0" }, { "a": "Site D", "b": "Site E", "c": "7.8" }, { "a": "Site D", "b": "Site C", "c": "2.8" }, { "a": "Site D", "b": "Site F", "c": "4.7" }, { "a": "Site D", "b": "Site B", "c": "4.5" }, { "a": "Site D", "b": "Site A", "c": "2.6" }, { "a": "Site E", "b": "Site D", "c": "7.9" }, { "a": "Site E", "b": "Site E", "c": "100.0" }, { "a": "Site E", "b": "Site C", "c": "2.8" }, { "a": "Site E", "b": "Site F", "c": "5.0" }, { "a": "Site E", "b": "Site B", "c": "4.1" }, { "a": "Site E", "b": "Site A", "c": "2.9" }, { "a": "Site C", "b": "Site D", "c": "0.6" }, { "a": "Site C", "b": "Site E", "c": "0.5" }, { "a": "Site C", "b": "Site C", "c": "100.0" }, { "a": "Site C", "b": "Site F", "c": "0.6" }, { "a": "Site C", "b": "Site B", "c": "0.8" }, { "a": "Site C", "b": "Site A", "c": "0.8" }, { "a": "Site F", "b": "Site D", "c": "3.0" }, { "a": "Site F", "b": "Site E", "c": "3.1" }, { "a": "Site F", "b": "Site C", "c": "1.8" }, { "a": "Site F", "b": "Site F", "c": "100.0" }, { "a": "Site F", "b": "Site B", "c": "3.2" }, { "a": "Site F", "b": "Site A", "c": "1.5" }, { "a": "Site B", "b": "Site D", "c": "1.8" }, { "a": "Site B", "b": "Site E", "c": "1.6" }, { "a": "Site B", "b": "Site C", "c": "1.4" }, { "a": "Site B", "b": "Site F", "c": "2.0" }, { "a": "Site B", "b": "Site B", "c": "100.0" }, { "a": "Site B", "b": "Site A", "c": "1.1" }, { "a": "Site A", "b": "Site D", "c": "5.4" }, { "a": "Site A", "b": "Site E", "c": "6.1" }, { "a": "Site A", "b": "Site C", "c": "8.0" }, { "a": "Site A", "b": "Site F", "c": "5.1" }, { "a": "Site A", "b": "Site B", "c": "6.1" }, { "a": "Site A", "b": "Site A", "c": "100.0" }]
            },
            aoa_duplication_table3: {
                "data": [{ "website": "Site", "pv": "PV", "uv": "UV Reach", "euv": "Exclusive UV Reach", "euvp": "Exclusive UV Reach%", "dup": "Duplication%" }, { "website": "Site D", "pv": "20288513", "uv": "12879363", "euv": "10767418", "euvp": "17.8%", "dup": "16.4%" }, { "website": "Site E", "pv": "16244659", "uv": "13061065", "euv": "10860559", "euvp": "18.0%", "dup": "16.8%" }, { "website": "Site C", "pv": "4904714", "uv": "2691112", "euv": "2294858", "euvp": "3.8%", "dup": "14.7%" }, { "website": "Site F", "pv": "14471608", "uv": "8200684", "euv": "6993508", "euvp": "11.6%", "dup": "14.7%" }, { "website": "Site B", "pv": "13498607", "uv": "5098765", "euv": "4282783", "euvp": "7.1%", "dup": "16.0%" }, { "website": "Site A", "pv": "77539266", "uv": "27331659", "euv": "25132345", "euvp": "41.7%", "dup": "8.0%" }]
            }

            //aoa end

        }



        return $rootScope.isEn ? en : zh
    }



})();
