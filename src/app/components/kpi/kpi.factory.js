(function() {
    'use strict';

    angular
        .module('adscan')
        .factory('aidSvc', aidSvc)
        .factory('cidSvc', cidSvc)
        .factory('pieRenderFcy', pieRenderFcy);

    /** @ngInject */
    function aidSvc($http) {
        return function() {
            return $http.get('https://willdevhost.herokuapp.com/myapp/listKPIaid/')
        }
    }
    /** @ngInject */
    function cidSvc($http) {
        return function(a) {
            return $http.get('https://willdevhost.herokuapp.com/myapp/listKPIcid/?aid=' + a)
        }
    }   


    function pieRenderFcy(){
        return pieRender
    }

    function pieRender(type) {
        [0, 1, 2, 3].forEach(function(i) {
            if (type == 'A') {
                pie("gender_canvasA", i)
            } else {
                pie("gender_canvasB", i)
            }
        })
    }

    function pie(name, id) {
        var colors = ["#d00000", "#0071d0", "#ff4e4e", "#5db5ff"]
        var imgs = [
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAABfCAMAAABY+f6WAAABxVBMVEX//////v7////+/Pz+/f39+/v68PD//v7////26en+/Pz+/f325+j9+vr15eb25ebz4eL68vL78vLz3t7z39/58PD58PHx2tvy2tvw2dnw2drx2Nnw19ju09Pv0tPu0dLu0dP04OH04eLtzc7rysvqycnw19jx19jpxcbowcLowcPt0NDu0NHnv7/nv8Dov8Htz9Dmvr/nvr/tzc7sy8zsy83lubrlubvkuLjjtbfktrjpxcbjs7TowsPjsrThr7DhsLLfqavfqqvep6jcoKLfq6zfq63bnqDfqavepqjYmZrdpKXYl5nYmJnYmJrZmJnZmJrcoqTXk5TVjY7XlZfTi47SiIrTiIrUjI7PgIPQgYPQgILNenzMd3nLcnXNeXzJcHPJcXPKcHPKcHTKcXTMdnnIbnDJcHPHbG7JbnDGZ2nEYWPDXmHCXV/CXWDDXWDCX2LBXF7CXF7BWFvAV1m+UVO9T1K8TVC7Sky6Rkm6Rkm6RkirISSrIiSsIiSsIiWsIyWsIyasJSetJCatJSeuKCquKCuvLC+wLC+wLTCxLjGyMTOyMzayNDezNDizNji0ODy1OTy1Oj21Oj62PT+4QUO4QkSpMQFBAAAAfHRSTlMAKystLS87QkJERERGRkhITU5OT09QUFNTVlZWWF1dX19gYGNmaGpqbG9vcHBxcXFyc3N0dnZ4eHp8fHx+foGDg4qKjJWVlZeXm5ydnp6enp6foqmrrLCwtbe3ucDCx8fJycnJycnNz9DT1Nvd39/f4eLi5+nt7/H2+Pn7tGhrwgAABEFJREFUSMeFl+eDFEUQxYs+lYUDcxZzVmbkxhwxx1vBjHlwxoy3mNPW74EgKqKI6e/1Q3fPzbGzZ3/YNK+rXr0K3Wt20hqNRqORrb82P77/GMf2P7Z5Hczo+u/AQXx/3Vx7o+2/4BIu+OnmebCzTjhiCjj66+w5pvbgxCVgz7CxxeMgeQL58cVB1JWghACmXDWIug0cgTuSo9sHUTuUOAlAvmMQdf6h7BEJHTpvOMbPc3gg+GKOYPfSg/n987R/uwd7d26Ktr7fMftw6/x0b3zyb8nhz6c2rlc5o8Xtz7z43K2L/1dh661wwbN7VyaTycrKZN9ksjJZmexbmex9+sLQx1z0ichLSXwAJhd3uHDfkV5uRMq3S+jIgwkWdh7uCislKMEdOPBAhG37EQnpJGhO+8/nmJmFlf6jmB1E2uPwcTCzy1GqgzW2RPourjCz16RVM51NVxfs9GWz8HXfWWaffxKgr4KF32Of9vjm98zgt2Ah+/e+MY9kk9tgAXrEVn2hLk6ChbjhlYeRpl1yHEAPfeQ4KKKAcQF9x4Ao62g6WIi7xxV4F7wkkBeNssdIZVdJfBKzJSnbcqfzuFzijsf2zmKVLWttjQt66U4FUbXSNMUIoN1FT4BsrKzjp2DBAWdc9fODC0HZxKg7W8tFz5MEYkripay9xqWvjoikemSvzpbYVeHqJUfSGlWjg10Fa5eAsvWOlyMxLj1Vg2drTtVGEzFGieUiN07HyymauCFYiPlbrly5Gj2FoaU6bgkWhCSWi5MbLebR6VfOcpkNebaVVU16AYxL9esZuZyiFQiPMeKMy0hb/f5dWq2JqHNErel/VDTx5IpKOOwu17RSfC1bJKEYI85yybR/pE2BadHGb8HCUQQ8Us04ROUb4HA0WPhAwA9nlt7v7tjK1T0AvBfMtnwrDj8aCklJqE7eYsPrB+GbLWZmp15z56ZgBR4z9OkNn2WflYVz77h6oTeBc4w0VmeC1cwsLz21Rmt1rthiFpUnQG11PpbLWZRSQ9bZFppFdT3UWtv144BHxYnVWttVziAvAWqsSUN7iH0ag3rH2lzVs7aqLHttbda+GvYo5LXVedAO6RUPFppV9kMoj8za6NGZDnkUPkXQJF4aslV5OmZrq30dVafuILXWZlQ1oEQSbF32VapiGms0Xy+lam9TFfqQraUseGN1GmIDtpbyRE315YMxFrl5Or0GayJJT21NGuUDqCLfAno1Uc7tDqUe0rBenuZXrx/LQe0ForYmXwSGYkznStP1kIrh3s680nViaALIFXm1eYpVw70NTmNNPkLmKCFQzBByVM2p+6x93FANTgDclbI9pwpvSpc1f8ua+B9F3DiD2pYpv2Av5TlxyQxq4Y/o5eCldlnq7X82zN5+7z4AQq8GC28i4PAtYeCSfNe/cOKJU8xs4flfxZfXhsG7dNh0+ob4JCyccVof8x92nApiCJcM5QAAAABJRU5ErkJggg==",
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAABfCAMAAABY+f6WAAABqlBMVEX////+/v7////8/f39/f76+/z7/Pz5+vvx8/Xu8fPv8fTm6u7k6e3i6Ozj5+zj6O3e4+jc4ufd4ujd4una4ObW3ePU2+LV3OPV3ePS2uHT2uHT2uLO1t7P1t7M1N3M1d3K0tzL09vL09zI0dnGz9nH0NnEztfFztnDzda+ydS8x9K9yNO6xtC4w864w8+5xdC3ws63w86zv8yxvsuuu8mvvcmtu8iqucerucentcSntsOhsMCiscGgsMCdrb2brL2ZqruZq7uVpriWp7iNn7KNoLOMnrGMnrKMn7KJnbCHm65/k6l/lKmAlap7kKZ7kKdziaFziqFyiaFrhJxshJxnf5logZpjfJdjfZdhe5VhfJVie5ZifJZfeZNdd5Jed5NWco1QbIpNaYdNaodNaohOa4hLaYZMaYZMaYdHZINFZIM+Xn48XHw6Wns3WHgzVHYzVXcyVHYuUXIsT3IJM1sKM1sKM1wKNFwLNFwLNF0MNVwNNV0ONl4ROGASOWASOWEVPWMWPWMWPmQXPmQYPmQbQWYdQmcdQ2gfRGkgRWkkSGwlSGwmSW0nS24wQVdhAAAAdHRSTlMAKiosLC4uMDg6OkJERkZGSkxMTE5SVFRUVlZWWlpcXF5eXmBiYmRkZmpsbG5wcHBycnZ4enp8fn6BgYeHiYuNj4+Tk5ubnZ2dn6GpqamtrbW1t729wcHFxcfHx8fJy8vT2dvb29vd3d3h4+vt7/H19ff7/ZlLqPEAAAR/SURBVBgZBcFL6+YFGQbg637e17GDOiCahU5oiyAIbFEmRG06LFq1KfoALaJvFlQLo0UUJEQHAkHE0MkyJcwamUZHR+fw/z131xUAgIEFAAQAuPTMF5668fDrr/zlLgAIAMynf3AfpBc//c8C4ASAefJHFz1VOT37xs0CMAC49OP3YwP94CeXAHACYL7zwDTl1Ca3Hvp7AQwA840uU03E1weAAeDyO5iOlub6AwAMAI+MKCD1CAADwJ1qV4TgAoAB4Np9JpnVETn/F4AB4PYNm4qsrXfvADAA7PPRFOD5BWAAcPUf0q4q/3wVACcA9PUnHhzBuPbzCwCcAOB42Vc+MPXY7567AIAAADNXnvzYR/96cxcAAAAAAAAB5lNffOz+S3eTTWdhzhfv/+/F6wsC8+j3Lu902rRJpWD2xi/fWZwwz/xwJFREBJrUJ569+1Y5MV/95m2z0kiSEoTEvafuvl3DZ751VzsQLdIY0XJ892FO5vuXmAbTBqSpOil3r7zU8cTDS1U0EZHCTEv7yBXjy4JJqo3OwkbbSpKnzTxuUEXURgkks/SzMx7vQkMIQAkbzRPG9RAlVCIiSEuk1w2JGK3VVlWNEgtGNevVP9OQSJWKP16TJIxUufWeIK3GmGpu3TFdMSRAU0YgBqJShq2AhGWwK6wQNYQ6V9DoRtOj7akFxoTmItrQEWFGHLGKsRtoKGkKbVATGNKE1ISGtqTmFCkMNHsWW7FF6GSxwxhGmwuQhTCydchs1Mga5tREE9MGMHWoNRKNvRCSzEZVmaRJxdmiTrFJFaJk92gjYqByYANpW0ZmkqkwEGjSIBlk2a2DZSA6pyQtDsLOIkZPNYA5HJUwNKaj1ZrlTGCFVJpKKyJCMSjd6WiYQ2u0TSuIoak5U+gGmoZo2tQZpbtKEzYVqZKlMQgiRHe3jTSNUyKZGg9p+KilJRIxQu8Ke9l4rTb3v3wuBpNudxfeaHDV7C/uyaU/vXuElG4Ngn3hteHOcxvue+qBqx/tl74tTXnvD1+7TJr69Utz+XM3Xz+cufc3OOhU+uFfn76cdlj2xgtwBjClalG2mRPAGSAtTVpgqgDOAExFRWEBYABObRtSaYJRgAE4VEIxbVgLMACGygiqKgAGYCp0VQ01LcAArNQhDS2JnAEGYGokAhXZHgADQDYwJKoC4AywVpV1KkIBDMAoBo6m2gIwAJmEjRKTFgADcJTUNmg7ADAArS4JmTQNAAYAMqKsFAEwAIy0dUJF2hPAACSrpg4UzRwAA3AKaYMmMRwFGICWTiTSqWVOAAOwLa0yljQAnAHGFEkdpFUBGAAC3UhoIgfAGYACKBEFcAZYJQpRCoABOImqQQsAGIBDBVuEpAowAIQKChUngAGgmEJKGgtwBhiwRAoVAANwJCOhgNEBOAOkqgCdTfYAGIANEkpMJ00BBuB6g3LLTVbKNYABuPZoKacXvVixse8ADEB/dgp19S3/frOkl35bgBNA3759ZfLo739z6GsPfv52PvzVKwUIAObj80EWJp+8d2cB+D8+9rYioGWs3gAAAABJRU5ErkJggg==",
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAABmCAMAAABoW+gHAAAChVBMVEX//////v7//////Pz//f3/+vr/+/v/+Pj/+fn/9vb/////9PT/9fT/9fX//v7/8vL/8/P/8PD/7u7/7+//8O//7Oz/7ez/7e3/7u3/6ur/6+r/5+f/6Oj/6ej/6en/6un/5ub/5+b/7+//8PD/5OP/5OT/4uL/4+L/7Oz/4OD/4eD/397/5+b/3Nv/3Nz/2tr/19f/39//1dX/3t3/09P/1dT/09L/z8//0ND/zc3/zs7/z87/ycn/ycj/xcX/xsX/xsb/x8b/zMz/w8P/xcT/y8r/y8v/zMv/wsL/wMD/wcD/v77/w8L/w8P/u7v/vLv/ubn/urn/urr/u7r/uLj/ubj/trX/u7r/u7v/s7P/tLP/tLT/uLj/s7L/tbT/rq3/q6v/rKv/raz/qan/qqn/qqr/q6r/qKf/qaj/raz/rq3/pqX/p6b/q6r/pKP/pKT/oqH/o6L/oJ//oaD/np3/oqH/nJv/oJ//mpn/m5r/mZf/mZj/mpj/lpX/l5X/l5b/lJP/lZP/lZT/lpT/kpH/lJL/kI//jo3/j43/kI7/kpD/k5H/jYz/joz/i4n/i4r/jIr/iYj/ioj/iIb/hYT/goH/g4L/gX//gYD/goD/goD/g4H/f37/fnz/e3n/d3X/eHb/dXP/dXT/d3X/dHL/dXP/cW//cnD/c3H/b23/cG7/cW//bWv/bmv/bmz/a2n/bGr/bWv/TEj/TEn/TUn/TUr/Tkr/Tkv/T0v/UEz/UU3/UU7/Uk7/VFH/VVL/VlP/WFX/WVb/Wlf/W1j/XFn/XVr/X13/YF7/YV//Yl//Y2D/Y2H/ZGH/ZGL/ZWL/ZWP/Z2T/Z2X/aGX/aGb/amj/a2n/bGl5UU3HAAAAsnRSTlMAMTE0NDc3Ojo8Pj8/P0BCQkVHR0dKSkpKTU1QUFBQUFJSUlJVVVhYWFpaXV1gYGNlZ2hqa2tucHBzc3N5e35+fn5+gYGBgYGEhoaJi4uMjI+Pj4+RkZSVlZeXl5ianZ+ioqKlpaWlp6enp6qqqq2tr6+ysrW3uLm6ur29vcDAwMPDw8PFxcjLy8vLy87O0NDQ09PW2dvb3t7e4ODj5ebs7e/v8PHy9PT19/f3+vr6/Pz8fo5TXwAABTFJREFUWMOVV4d/FEUU3n2JhBwGCdgimIslkSiiImJB1AgWUGMFDYrhLIkFo1xQQaXYwQJRIoolGI1iIajzhlgwiZqiESyI+vc4M+/t7Oxefrfr5JfbvZ1v3nzve2X2PG+ckdHDSzcmX/v2MA6/c93kFNjMrM+EGfj5rGT7M0fRYNX/6MxE0+8bIME/TDJ+JUohZIC/PAH9kkGReSFeTiDyERumz08S0P8wB7L+VQL6PWtXr3o3gff9xnTA+4EE9CmDxFqTwYGTk/S+gxWU6u/OxGBmHkUhicvjKTIrc9WXqNH/XpEuDTPn3PbQreemztk0A+puWJVvVyOfV5c8XVYtPQ3Gw17UI0WYIXQ17vZcUIAvfaKPIfoDbbKglvKp0pjlDtcs2hyXdPNKxDosNbELp8OspbA2uvAJIxobmndz3IyRCQ56YUjTLHJXmi/iMge9MQChtWtoCbtsnUP7g2DeSW6GsSO7QuL+YAwSqoJMaNC36JPY/WCKPHAdRTHDohuIAG5ZY70K9lmzhbktsOhWnpxU6+xNo3YKr7vHOrmdZn6BhYYxht7i1fAzEXo9cBMO6QdS7IRHRGSoFavhLbo/FKDLviEN8vBGEA+rzE5oJ4X6JjL6LDa1BMZCDU1whBiDxUYhFLMZfSMj6ioIJjl1pYYdWc/Lr2f0WgLtL5lrw06C62KYW/I9EePYQxdt3QvL40FRown2ErqL3IQhSobN3nNWC6OiWfW89yI9HCL0MRzoZtjtJEdQdbvhLo7X8QZ9IUtwvt/PKSetLEL2+/M5LecbdDPH7bjaKGNeVjtVz6uMazbozSaQitclTsoGFSnEIhim4L1qnOw1aYddsNomoHTQbayZ3KvdLPmOdnwMdpDCJiq2dMQOWEd77S9R6Ho22AgHRLx49f9BaOS70xV6MQPOrEBLAq2TqlNVzKa1uESh8zTz9cQ5aLtDUOzG//PK+gjSrpzk/D0MTZHjLKgIxNvhL3r+JngwStPb4emw6KkVctFtgteI2Rh4XHfYCj1h45HspKmLbriPrUzx5rHBi/1+DFqDjISp31/Ad/O85Zw/M2oKkjVYUjOd+9wK7xmyNOAvsn1KShnJ80tLB6lXr/e20ZNuaGNVRViW3EUfhl0Uha3e3cqWerYSOmMM1BZIB3gnPEg73etN+1HbGDoaDjpMZWTlb1A5rK8/TPOgfg+Kj+ugXEoUBYeaWbqvHOo+FeLPM3QSwvQT1GWOiGa3++ZxtgKdeKrvnD3Lwu6OtkPQF7Gs4HTdZOqJT0qMElpfcBR383sGOgea5MB2xw9jf4BUQyygL3DAj6FrXBnskcW1gTUxdIPbtJ2jhz4bYug2i5RoGhvaqlD3rTEnO219SfcIZiadUTfhd0p+DHLEaVZq/BFFl9sOHz1IeI995RF0ZUtLrqWlJW+yFEV7rpds9+RyOTWzsnK8V6ssE6jyOsj4xmIvYlXsWZUqkvFjXoBWOI02jBPQpEqVt5WWbUi2bZhQLIvazrJuKXmz7KyJwGQvFT5rGkeyl4HtbeaQSFZQR1szkWlsO7HEJAU5tapNdJI0yUqrdwp0FTpovcuGFF4aTdJllW4hge1UaBWdDqqb1BmL6SKvdKnWsUzM2IJKS7BNac1epuId6i1S621ESYw8hral+F9MUvHGtJrY6Mg0mlASZqmfiOKdrZpbq2IiTZ8vansqt/ljvRfoiHuyGNofMQr+VOo1EZObiv7YvcUwWQFe2Rd6kz1HFP9tfM3f3x6+WZ8bRz3764G1k2LT/wFOL5luh7/TMwAAAABJRU5ErkJggg==",
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAABmCAMAAABoW+gHAAACeVBMVEX////////9/v/9///+/v/+///7/f/7/v/8/v/4+//5/P/2+v/3+//0+//1+v/1+//y+f/z+f/z+v/w+P/+/v/////v+P/s9v/t9//q9f/r9f/r9v/o9P/o9f/p9P/p9f/m9P/n9P/x+P/k8//l8//l9P/v9//i8f/i8v/j8v/j8//t9//h8v/e7//f8P/p9f/d7//o9f/Y7f/Y7v/Z7v/h8v/W7P/W7f/X7f/g8P/S6v/S6//T6//Q6v/M5//M6P/N6P/K5//L5v/L5//I5v/J5v/J5//Q6v/H5f/H5v/O6P/E4//F5P/F5f/A4v/A4//B4//H5f/H5v++4v+/4v+74P+74f/B4v/A4/+43/+53/+54P++4f+73/+03P+03f+y3P+z3P+w2/+w3P+x2/+u2v+v2v+z3P+s2f+s2v+t2f+x2/+y3P+q2P+r2f+o2P+p2P+m1/+n1/+n2P+q2f+l1v+n1/+i1P+i1f+j1f+g0/+h1P+h1f+e0v+e0/+f0/+c0v+c0/+d0v+d0/+b0f+Z0P+b0f+Z0f+Wz/+W0P+X0P+Uzv+Uz/+Vz/+Szf+Tzv+QzP+Oy/+OzP+PzP+Ny/+NzP+Kyv+Lyv+Myv+Iyf+GyP+Ex/+Fx/+Bxv+Axf+Bxv9/xf+Axf9+xP9+xf9/xP9/xf99xP98xP97w/96w/97w/9Ys/9YtP9Zs/9ZtP9atP9btf9dtf9dtv9gtv9gt/9ht/9huP9it/9juP9kuf9luf9muf9muv9nuf9ouv9pu/9qu/9qvP9rvP9svf9tvf9uvf9uvv9vvv9wvf9wvv9yvv9yv/9zv/90wP91wP92wP92wf93wf94wv8HOLV1AAAAq3RSTlMANjk5OTk8PDw/P0JCRUVFSEhIS0tLTlFRVFRUV1dXV1paW11dXV5gYGBgYWNmZmZpaW9vb3FycnJzeHh4e4GBgYSEhIeHh4mKioyNjY2Tk5OUlJaWnJycnp+fn6GkpaWoqKurq66urrGxsbGxtLS3t7q6ury9v8DAwMPDw8bGxsnJycnMz8/R0tLS1dXV2Njb3t7e4eHk5OTq7O3t8vPz9PT29vb29/n6/PyMY3DtAAAFwElEQVQYGa3B+b+m9RzH8c83M8mUmky3q+aWIkuKLFkiJEtZSlGWCiUnFDo5liR3k4Ryq0NkkLWJ4vXuNRlTZKKUZEnM/EWu+5ym5qTHNf3g+azHM5qpJ+aAd39/m9uuf9cBtXujl94CGLzlxaPanSPvFUnQe46s3Rj9mJ4Q8OejGnYiEkBmTqxhXweBBNSv1aDRz+gJhN4vRzVktB1BQXrbRzVk9ENRElD8wagGfVIMPdF8rIY9+3aMoOgdz6thow8IGBU/OKrdGH3WGFE/P6rdGp243QD/efuonoDR6BXv+8T7Xz0a1f9PO+z08z4zv7CwML+wML+wsDA/v7CwcMHph7X6X+3YGxEQMPQUQW58bavH2OOLmwkIUQiIIphbv7RHrdCuYYkiMiPKTPCaVrtopymiCAoGUJEIvLPVo/a8BxBBQDQBJIaZu/esR70ZBRF7gCGASk89vh61ARRRQRAVAelJLq5HtE0YAQOhJyooD/tJq51WbwWCaBARJCqEnvxude10CGAPMAgCgoBExGfWTq+HKHzjc8pMAAH59NVIIsfVTheAIvseqgQkzCjPeRqIcm49rH0HQf/S3oYziAkCb233AsK1rZa1BzHCxnYRywIIiRe1jfT0wVbL1mwWIQttIygiYEQ3tnkF3bymlr2EgPHkdh8yExEQ7msn4UxeVMtON8Rw2H5gL2APBPZ7Phjk1Fp2cTC6Za9jgkFBUOwds9cWAnhJLWk3sGRHOytBQMJOZ7UdBMOPWs20uwKaq9qVEkQFJWiubFfJzLZWMwciKGe3mwEVhUQQbm4fDkvW1czriJgcu2qrAcVATMRsXfUagsFja+ZDASHrDiWICIEoGA5dFzV4dvXaVQLxrvYWIwlIRMDeCe0uCXB1q6q2AwRuaBcaEUHEBJBPtRuIkh2tqp68RSJ+oV0v9iIgPel9t10iM1v2qqrDQXqntr9qIAlKFAQeaKcaRI+oqpNB0KP2CxIQAigCbn7qUYLKSVW1ACTetuZlogERFEF6Rz/lNkCdr2obAcM/2hkaEkABAUXObA/S0++1an8OiN9qlwMqIqIGQvLldm2I5L5W+4sK57WbQBGjgIr0NrWPIgT2r1cFEd6waqtElBkJCMhvVx2HSeSVdUZQ8KBnYZAlAREF4eCDRJEz6goDePuTTmDGXgyKIRA4fvXvNcIVNTUIv2jnCxIIioigwPntpyo4rY8IyFz7NssElyCoeF37uPQ8t9bdSe9Pa9v9hAgqJICBCA+0tX+kd+e6ai/cHn59RNvn1oj0EqSnib1s3ru94Ffwz8NbVWvPOLBVHQ2IgkgQe4Dw8qo2fu7qVo94Lz17KAgqIoT31GNdjr0AIoGALLusHqNt0kQQBBIVpOemViutuiP2ICCIIEQjW1fXSgcLoiARBaSnwCG10puIoAGiYICoGN9YK12oQnwYSxSXXFArtOtE1BBAEYRgj+ta7ar93R4homAAZZl/a7WrfW4NUdAIoggKSNy7drV2rnfO3DyKMD+3g565ae6cublz5ubW1uMYBwh2bRoVJzVgPYbI+loMPS+rAR0YoKup9JzUgI6eMK5pxGRSA9bTU7taFAiTGtAZQce1mBCd1IAuJGpXUxGY1IAuAqarqYBuqAEdy7qaKr1JDRgrRruaGpBJDehCELtaVJRJDeiIIl1NA4RJDegwCF1NSdBJDejohYxrShQnNWCsgHY1ZcZJDVgviHY1JUA21IDOBHBcUxSZ1IBOpNfVFAQmNaBDQLpaRNRJDRgTELtaRDCTGtApoOOaosCkBnRIkHEtSoRJDRgrxHT1TZQwqQFjIcGuFpHepAZ0YIhdTUVgUgM6QbCrKdKb1IBORehqihImNaBDZrqaCpINNWA9Yq+rKUq4rAY8XZk5sL6KRC6tAavuDsof9qizQJJTakA7zahntlrzEOK/96wh7R0P/eZfp7SqtvYr9z9w6b6tVvgvovNl6bBnzQwAAAAASUVORK5CYII="
        ]
        var canvas = document.getElementById(name + id)
        var context = canvas.getContext('2d')
        var w = canvas.clientWidth
        var h = canvas.clientHeight
        var per = canvas.getAttribute('data-value') / 100

        var x = w / 2
        var y = h / 2
        var lineWidth = 5;
        var r = Math.min(x, y) - lineWidth
        context.lineWidth = lineWidth;
        context.beginPath();
        context.arc(x, y, r, 0, 2 * Math.PI, false);
        context.strokeStyle = '#b9b9b9';
        context.stroke();


        var img = new Image()
        img.src = imgs[id]

        img.onload = function() {
            var iw = img.width * 0.8
            var ih = img.height * 0.8
            context.drawImage(img, (w - iw) / 2, (h - ih) / 2, iw, ih)
        }
        context.beginPath();
        context.arc(x, y, r, 0, per * 2 * Math.PI, false);
        context.lineWidth = lineWidth;
        context.strokeStyle = colors[id];
        context.stroke();
    }



})();
