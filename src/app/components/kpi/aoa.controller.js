(function() {
    'use strict';

    angular
        .module('adscan')
        .controller('AoaController', AoaController);


    function AoaController($http, url, $cookies, Resource, demoData) {
        var vm = this;
        var userInfo = $cookies.getObject('userInfo');
        var isAdminDemo = false

        vm.uvIsShow = true;
        vm.du_color = ['#bc8aff', '#9994fd', '#64adff', '#4cc3fe', '#58daff', '#5df1fd'];
        vm.render = function(aid, cid) {
            vm.aid = aid;
            vm.cid = cid;


            //destroy
            vm.dupliction = []
            vm.du_table1 = []
            vm.du_table2 = []
            vm.du_table3 = []

            if (/demo/i.test(userInfo.username)) {
                demoShow()
                return false;
            }
            if (userInfo.username == 'admin' && !isAdminDemo) {
                demoShow()
                isAdminDemo = true
                return false;
            }

            //restart

            uvreach()
            dupliction()
            du_table1()
            du_table2()
            du_table3()
        }

        function demoShow() {
            vm.uvIsShow = 1
            hcPie(handleUv(demoData['aoa_uvreach'].data))
            vm.dupliction = handledupliction(demoData['aoa_duplication'].data)

             vm.du_table1 = hanleTable(demoData['aoa_duplication_table1'].data, '(重合人数)')
             vm.du_table2 = hanleTable(demoData['aoa_duplication_table2'].data, '(重合人数)')
              vm.du_table3 = demoData['aoa_duplication_table3'].data.slice(1)
        }

        function uvreach() {
            Resource
                .getKpiData('aoa_uvreach', { cid: vm.cid.id, aid: vm.aid.id })
                .then(function(data) {
                    if (data.data.length === 0) {
                        vm.uvIsShow = 0;
                        return false;
                    } else {
                        vm.uvIsShow = 1
                        hcPie(handleUv(data.data))
                    }
                }, function(er) {
                    vm.uvIsShow = false
                    console.log(er)
                })
        }


        function dupliction() {
            Resource
                .getKpiData('aoa_duplication', { cid: vm.cid.id, aid: vm.aid.id })
                .then(function(data) {
                    vm.dupliction = handledupliction(data.data)
                }, function(er) {
                    vm.dupliction = []
                    console.log(er)
                })

        }

        function du_table1() {
            Resource
                .getKpiData('aoa_duplication_table1', { cid: vm.cid.id, aid: vm.aid.id })
                .then(function(data) {
                    vm.du_table1 = hanleTable(data.data, '(重合人数)')
                }, function(er) {
                    vm.du_table1 = []
                    console.log(er)
                })
        }

        function du_table2() {
            Resource
                .getKpiData('aoa_duplication_table2', { cid: vm.cid.id, aid: vm.aid.id })
                .then(function(data) {
                    vm.du_table2 = hanleTable(data.data, '(百分比)')
                }, function(er) {
                    vm.du_table2 = []
                    console.log(er)
                })
        }



        function du_table3() {
            Resource
                .getKpiData('aoa_duplication_table3', { cid: vm.cid.id, aid: vm.aid.id })
                .then(function(data) {
                    vm.du_table3 = data.data.slice(1)
                }, function(er) {
                    vm.du_table3 = []
                    console.log(er)
                })
        }


    }

    function handledupliction(ar) {
        if (ar.length === 0) return [];
        var sortedar = _.sortBy(ar, function(v) {
            return v.value * 1
        })
        var max = parseFloat(sortedar[ar.length - 1].value)
        return sortedar.map(function(v) {
            v.per = parseFloat(v.value) * 50 / max
            return v
        })
    }

    function hcPie(dataAr) {
        Highcharts.chart('container', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45
                }
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    innerSize: 120,
                    depth: 45
                }
            },
            series: [{
                name: 'Delivered amount',
                colors: ["#d00000", "#0071d0", "#ff4e4e", "#5db5ff", '#bc8aff', '#9994fd', '#64adff', '#4cc3fe', '#58daff', '#5df1fd'],
                data: dataAr
            }]
        });
    }


    function handleUv(ar) {

        return ar.map(function(v) {
            return [v.name, parseFloat(v.value)]
        })
    }


    function hanleTable(data, str) {
        var obj = {}
        var orderKey = []
        var end = []

        data.forEach(function(o) {
            obj[o.a] = obj[o.a] || {}
            obj[o.a][o.b] = o.c

            obj[o.b] = obj[o.b] || {}
            obj[o.b][o.a] = o.c

            orderKey = _.sortBy(_.keys(obj))
        })

        orderKey.forEach(function(vo, i) {
            var ar = []
            ar.push(vo)
            orderKey.forEach(function(vi) {
                ar.push(obj[vo][vi])
            })

            end.push(ar)
        })
        orderKey.unshift(str)
        end.reverse().push(orderKey)

        // console.log(end)
        return end
    }




})();
