(function() {
    'use strict';

    angular
        .module('adscan')
        .filter('adperFlt', adper);

    function adper() {
        return function(input) {
            return input * 1 ? input + '%' : input
        }
    }
})();
