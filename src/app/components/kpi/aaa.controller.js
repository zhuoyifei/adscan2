(function() {
    'use strict';

    angular
        .module('adscan')
        .controller('AaaController', AaaController);

    function AaaController($http, $scope, demoData, Resource, $cookies, url, pieRenderFcy) {
        var vm = this
        var isAdminDemo = false
        var userInfo = $cookies.getObject('userInfo')

        vm.itemShow = 1


        vm.db_aid = vm.db_cid = null
        _.forEach(['income', 'age', 'edu'], function(n) {
            vm[n + 'A'] = true
            vm[n + 'B'] = true
        })

        vm.renderTab = function() {
            if (vm.db_aid && vm.db_cid) {
                vm.render(vm.db_aid, vm.db_cid)
            }
        }

        vm.render = function(aid, cid) {
            var type = vm.itemShow == 1 ? 'A' : 'B'

            vm.aid = vm.db_aid = aid
            vm.cid = vm.db_cid = cid
            vm.canvasDataA = []
            vm.canvasDataB = []

            if (/demo/i.test(userInfo.username)) {
                demoShow(type)
                return false;
            }
            if (userInfo.username == 'admin') {
                if (vm.cid.id == 'demo') {
                    demoShow(type)
                    return false;
                }
            }

            gender(type)
            income(type)
            age(type)
            edu(type)
        }

        function demoShow(type) {
            vm['canvasData' + type] = demoData['aaa_gender_' + type.toLowerCase()].data
            vm['income' + type] = vm['age' + type] = vm['edu' + type] = 1
            setTimeout(function() {
                pieRenderFcy(type)
                hcRender("hc-income" + type, handleHcData(demoData['aaa_gender_' + type.toLowerCase()].data));
                hcRender("hc-age" + type, handleHcData(demoData['aaa_age_' + type.toLowerCase()].data), 'spline');
                hcRender("hc-edu" + type, handleHcData(demoData['aaa_edu_' + type.toLowerCase()].data))
            }, 0)
        }

        function gender(type) {
            Resource
                .getKpiData('aaa_gender_' + type.toLowerCase(), { cid: vm.cid.id, aid: vm.aid.id })
                .then(function(data) {
                    vm['canvasData' + type] = data.data
                    setTimeout(function() {
                        pieRenderFcy(type)
                    }, 0)
                }, function(er) {
                    vm['canvasData' + type] = []
                    console.log(er)
                });
        }

        function income(type) {
            Resource
                .getKpiData('aaa_income_' + type.toLowerCase(), { cid: vm.cid.id, aid: vm.aid.id })
                .then(function(data) {
                    vm['income' + type] = 1
                    hcRender("hc-income" + type, handleHcData(data.data))
                }, function(er) {
                    vm['income' + type] = 0
                    console.log(er)
                })
        }

        function age(type) {
            Resource
                .getKpiData('aaa_age_' + type.toLowerCase(), { cid: vm.cid.id, aid: vm.aid.id })
                .then(function(data) {
                    vm['age' + type] = 1
                    hcRender("hc-age" + type, handleHcData(data.data), 'spline')
                }, function(er) {
                    vm['age' + type] = 0
                    console.log(er)
                })

        }

        function edu(type) {
            Resource
                .getKpiData('aaa_edu_' + type.toLowerCase(), { cid: vm.cid.id, aid: vm.aid.id })
                .then(function(data) {
                    vm['edu' + type] = 1
                    hcRender("hc-edu" + type, handleHcData(data.data))
                }, function(er) {
                    vm['edu' + type] = 0
                    console.log(er)
                })
        }
    }

    function hcRender(container, opt, type) {
        Highcharts.chart(container, {
            chart: {
                renderTo: container,
                type: type ? type : 'column'
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: opt.categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: null
                },
                labels: {
                    formatter: function() {
                        return this.value + ' %';
                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: opt.series
        });

    }


    function handleHcData(data) {
        var colors = ["#d00000", "#0071d0", "#ff4e4e", "#5db5ff"]
        var groupByName = _.groupBy(data, function(v) {
            return v.name
        })
        var groupByType = _.groupBy(data, function(v) {
            return v.type
        })
        var categories = _.keys(groupByName)
        var names = _.keys(groupByType)
        var series = names.map(function(n, i) {
            var dataAr = []
            data.forEach(function(v) {
                if (v.type == n) {
                    dataAr.push(v.value)
                }
            })
            return { name: n, data: dataAr, color: colors[i] }
        })
        return { categories: categories, series: series }
    }

})();
