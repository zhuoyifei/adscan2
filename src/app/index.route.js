(function() {
    'use strict';

    angular
        .module('adscan')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        // console.log()
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainController',
                controllerAs: 'main',
                redirectTo: function(ui, navCf) {
                    var me = this;
                    if (navCf.isMp) {
                        me.state.go('home.mp')
                        return false;
                    }
                    if (navCf.isMsPlan) {
                        me.state.go('home.msplan')
                        return false;
                    }
                    // if (navCf.isKpi) {
                    //     me.state.go('home.kpi')
                    //     return false;
                    // }
                    // if (navCf.isGrp) {
                    //     me.state.go('home.grp')
                    //     return false;
                    // }
                    // if (navCf.isFt) {
                    //     me.state.go('home.ft')
                    //     return false;
                    // }
                    if (navCf.isDmp) {
                        me.state.go('home.dmp')
                        return false;
                    }

                    alert('无任何权限')
                    me.state.go('login')

                },
            })
            .state('policy', {
                url: '/policy',
                templateUrl: 'app/auth/policy.html',
            })
             .state('noauth', {
                url: '/noauth',
                templateUrl: 'app/auth/noauth.html',
            })
            .state('login', {
                url: '/login',
                templateUrl: 'app/auth/login.html',
                controller: 'LoginController',
                controllerAs: 'login'
            })
            .state('home.sysadmin', {
                url: 'sysadmin',
                templateUrl: 'app/components/sysadmin/sysadmin.html',
                controller: "SysadminController as sa",
            })
            .state('home.mp', {
                url: 'mediaplanning',
                templateUrl: 'app/components/mp/mp.html',
                redirectTo: 'home.mp.demographic'
            })
            .state('home.mp.demographic', {
                url: '/demographic',
                templateUrl: 'app/components/mp/demographic.html',
            })
            .state('home.mp.web-surfinghabits', {
                url: '/web-surfinghabits',
                templateUrl: 'app/components/mp/web-surfinghabits.html',
            })
            .state('home.mp.mediahabits', {
                url: '/mediahabits',
                templateUrl: 'app/components/mp/mediahabits.html',
            })
            // .state('home.ft', {
            //     url: 'faketracking',
            //     templateUrl: 'app/components/ft/ft.html',
            // })
            .state('home.cpp', {
              url: 'cpp',
              templateUrl: 'app/components/cpp/cpp.html',
              controller: "CppController as cpp"
            })
            .state('home.dmp', {
                url: 'dmp',
                templateUrl: 'app/components/dmp/dmp.html',
                redirectTo: 'home.dmp.user'
            })
            .state('home.dmp.user', {
                url: '/user',
                templateUrl: 'app/components/dmp/user.html',
            })
            // .state('home.kpi', {
            //     url: 'kpi',
            //     templateUrl: 'app/components/kpi/tmpl/kpi.html',
            //     redirectTo: function(ui) {
            //         console.log('biting')
            //         console.log(ui)
            //         var me = this;
            //         var ar = [ui.p1apa, ui.p1aaa, ui.p1rca, ui.p1aoa]
            //         var sar = ['apa', 'aaa', 'rca', 'aoa']
            //         if (ui.role == 'admin') {
            //             me.state.go('home.kpi.apa')
            //             return false;
            //         }
            //         for (var k = 0; k < ar.length; k++) {
            //             console.log('ar[k]', ar[k])
            //             if (ar[k] == '1') {
            //                 console.log('kpi go to ', sar[k])
            //                 me.state.go('home.kpi.' + sar[k])
            //                 break;
            //             }
            //         }

            //     }
            // })
            // .state('home.kpi.apa', {
            //     url: '/apa',
            //     templateUrl: 'app/components/kpi/tmpl/apa.html',
            //     controller: 'ApaController',
            //     controllerAs: 'apa'
            // })
            // .state('home.kpi.aaa', {
            //     url: '/aaa',
            //     templateUrl: 'app/components/kpi/tmpl/aaa.html',
            //     controller: 'AaaController',
            //     controllerAs: 'aaa'
            // })
            // .state('home.kpi.rca', {
            //     url: '/rca',
            //     templateUrl: 'app/components/kpi/tmpl/rca.html',
            //     controller: 'RcaController',
            //     controllerAs: 'rca'
            // })
            // .state('home.kpi.aoa', {
            //     url: '/aoa',
            //     templateUrl: 'app/components/kpi/tmpl/aoa.html',
            //     controller: 'AoaController',
            //     controllerAs: 'aoa'
            // })
            // .state('home.grp', {
            //     url: 'grp',
            //     templateUrl: 'app/components/grp/tmpl/grp.html',
            //     redirectTo: function(ui) {
            //         var me = this;
            //         var ar = [ui.p1tp, ui.p1ap, ui.p1rc]
            //         var sar = ['tp', 'ap', 'rc']
            //         if (ui.role == 'admin') {
            //             me.state.go('home.grp.tp')
            //             return false;
            //         }
            //         for (var k = 0; k < ar.length; k++) {
            //             if (ar[k] == '1') {
            //                 me.state.go('home.grp.' + sar[k])
            //                 break;
            //             }
            //         }

            //     }
            // }).state('home.grp.tp', {
            //     url: '/tp/:id',
            //     templateUrl: 'app/components/grp/tmpl/tp.html',
            //     controller: 'TpController',
            //     controllerAs: 'tp',
            //     resolve: {
            //         RpIdSvc: function($stateParams) {
            //             return $stateParams.id
            //         }
            //     }
            // }).state('home.grp.ap', {
            //     url: '/ap/:id',
            //     templateUrl: 'app/components/grp/tmpl/ap.html',
            //     controller: 'ApController',
            //     controllerAs: 'ap',
            //     resolve: {
            //         RpIdSvc: function($stateParams) {
            //             return $stateParams.id
            //         }
            //     }
            // }).state('home.grp.rc', {
            //     url: '/rc/:id',
            //     templateUrl: 'app/components/grp/tmpl/rc.html',
            //     controller: 'RcController',
            //     controllerAs: 'rc',
            //     resolve: {
            //         RpIdSvc: function($stateParams) {
            //             return $stateParams.id
            //         }
            //     }
            // })
            .state('home.grp.reportlist', {
                url: '/reportlist/:listtype',
                templateUrl: 'app/components/grp/tmpl/rlist.html',
                controller: 'ReportlistController',
                controllerAs: 'rls',
                resolve: {
                    listTypeSvc: function($stateParams) {
                        var t = $stateParams.listtype
                        return ['tp', 'rc', 'ap'].indexOf(t) == -1 ? 'tp' : t;
                    }
                }
            }).state('home.msplan', {
                url: 'msplan',
                templateUrl: 'app/components/msplan/msplan.html',
                redirectTo: function(ui) {
                    var me = this;
                    if (ui.crossreach_online_tv) {
                        me.state.go('home.msplan.tv')
                    } else {
                        me.state.go('home.msplan.pc')
                    }
                }
            }).state('home.msplan.tv', {
                url: '/onlinetv',
                templateUrl: 'app/components/msplan/tv.html',
                controller: "OnlineTvController as tv"
            }).state('home.msplan.pc', {
                url: '/pcmobile',
                templateUrl: 'app/components/msplan/pc.html',
                controller: "PcMobileController as pc"
            });

        $urlRouterProvider.otherwise('/');
    }
})();
