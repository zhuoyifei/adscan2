(function() {
  'use strict';

  angular
    .module('adscan', ['ngCookies','angularFileUpload','ui.bootstrap', 'ui.router','toastr','smart-table','ui.select']);
 
})();
