(function() {
    'use strict';

    angular
        .module('adscan')
        .run(runBlock);

    /** @ngInject */
    function runBlock($window, $rootScope, $state, $location, $cookies, navAuth) {
        var unAuth_link = ['/policy']
        var init_link = $location.path();
        var userInfo = $cookies.getObject('userInfo');
        var _path = unAuth_link.indexOf(init_link) === -1 ? '/login' : init_link;

        userInfo ? $rootScope.userInfo = userInfo : $location.path(_path);

        navAuth.updateCf()

        $rootScope.$on('$stateChangeStart', function(evt, to, params) {
            var route_ar = to.name.split('.');
            var _key = 'is' + _.capitalize(route_ar[1]);
            var navCf=navAuth.getCf();
            var isHaveAuth = navCf[_key];
            var userInfo=$rootScope.userInfo;
                //验证登录
                console.log('lll',_key)
            if (!userInfo) {
                to.name !== 'login' && $location.path(_path)
                return;
            }
            //验证一级导航权限
            if ((_key in navCf) && !isHaveAuth) {
                // alert('无权限，请联系管理员!');
                evt.preventDefault();
                $state.go('noauth');
                return;
            }
            //重定向
            if (!to.redirectTo) return;
            evt.preventDefault();
            typeof to.redirectTo == 'string' ? $state.go(to.redirectTo, params, { location: 'replace' }) : to.redirectTo.call({ state: $state }, userInfo, navCf)

        });

        $rootScope.lang = $cookies.get('lang') || 'zh'
        $rootScope.isEn = $rootScope.lang == 'en'

        $rootScope.changeLang = function(nlang) {
            if (nlang == $rootScope.lang) return false;
            $rootScope.lang = nlang;
            $rootScope.isEn = $rootScope.lang == 'en';
            $cookies.put('lang', nlang)
        }


    }
})();
