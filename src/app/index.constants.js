(function() {
    'use strict';

    angular
        .module('adscan')
        .constant('url', {
            host: 'https://willdevhost.herokuapp.com',
            api:'http://data.ad-rating.com/adscangrp/api.jsp',
            api2:"http://adone.comratings.com/adscangrp/api.jsp",

            // apa start
            listAid: '/myapp/listKPIaid',
            listCid: '/myapp/listKPIcid',
            detail: "/myapp/apa_detail",
            trend: '/myapp/apa_trend',
            effect: "/myapp/apa_effect",
            progress_plan: "/myapp/apa_progress_plan",
            progress_coast: "/myapp/apa_progress_cost",
            progress_target_cpm: "/myapp/apa_progress_target_cpm",
            progress_target_ctr: "/myapp/apa_progress_target_ctr",
            topsite: "/myapp/apa_topsite",
            topregion: "/myapp/apa_topregion",
            siteeffect: "/myapp/apa_siteeffect",
            // apa end

            //aaa start
            aaa_gender_a: '/myapp/aaa_gender_a',
            aaa_income_a: '/myapp/aaa_income_a',
            aaa_age_a: '/myapp/aaa_age_a',
            aaa_edu_a: '/myapp/aaa_edu_a',
            aaa_gender_b: '/myapp/aaa_gender_b',
            aaa_income_b: '/myapp/aaa_income_b',
            aaa_age_b: '/myapp/aaa_age_b',
            aaa_edu_b: '/myapp/aaa_edu_b',
            //aaa end

            //rca start
            apa_siteeffect_rca: '/myapp/apa_siteeffect_rca',
            //rca end

            //aoa start
            aoa_uvreach: '/myapp/aoa_uvreach',
            aoa_duplication: '/myapp/aoa_duplication',
            aoa_duplication_table1: '/myapp/aoa_duplication_table1',
            aoa_duplication_table2: '/myapp/aoa_duplication_table2',
            aoa_duplication_table3: '/myapp/aoa_duplication_table3'
            //aoa end
        })


})();
