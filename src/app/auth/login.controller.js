(function() {
    'use strict';

    angular
        .module('adscan')
        .controller('LoginController', LoginController);

    function LoginController($log, $rootScope, $location, $state, $scope, Auth, toastr, $cookies,navAuth) {
        var vm = this;
        vm.IsDisable = false;

        vm.username = $cookies.get('re_username')
        vm.password = $cookies.get('re_password')

        vm.submit = function() {
            vm.IsDisable = true;

            $rootScope.BIlogin = "http://comscan.comsmartgroup.com/api?action=login&adminv=" + vm.username + "&passv=" + vm.password
            console.log($rootScope.BIlogin)
            var _iframe = document.getElementById('loginFrame')
            _iframe.src = $rootScope.BIlogin

            Auth.login(vm.username, vm.password).then(function(data) {
                $rootScope.userInfo = data;
                console.log(data)
                $cookies.put('re_username', data.username)
                $cookies.put('re_password', data.password)
                navAuth.updateCf()
                $state.go('home')
            }, function(er) {
                vm.IsDisable = false;
                toastr.error(er)
            })
            setTimeout(function() {
                vm.IsDisable = false
            }, 0)

        }


    }
})();
