(function() {
    'use strict';

    angular
        .module('adscan')
        .factory('navAuth', navAuth);


    function navAuth($rootScope) {
        var navCf = {
            isAdmin: false,
            isMp: false,
            isCpp:false,
            isMsplan: false,
            isKpi: false,
            isGrp: false,
            isFt: false,
            isDmp: false,
            isSysadmin:false
        }

        function varify(v) {
            return v === '1'
        };

        function set() {
            var ui=$rootScope.userInfo
            if (ui) {
                navCf.isAdmin=navCf.isSysadmin = ui.role === 'admin'
                if (navCf.isAdmin) {
                   navCf.isSysadmin=navCf.isCpp= navCf.isMp = navCf.isMsplan = navCf.isKpi = navCf.isGrp = navCf.isFt = navCf.isDmp = true
                } else {
                    navCf.isMp = _.some([ui.mediaplanning_demographic, ui.mediaplanning_web_habits, ui.mediaplanning_media_habits], varify);
                    navCf.isCpp = _.some([ui.cpp_media_penetration,ui.cpp_search_engines,ui.cpp_ad_penetration,ui.cpp_website_penetration,ui.cpp_search_key_penetration,ui.cpp_search_brand_penetration,ui.cpp_shop_path,ui.cpp_shop_penetration], varify);
                    navCf.isMsplan = _.some([ui.crossreach_online_tv, ui.crossreach_pc_mobile], varify);
                    navCf.isKpi = _.some([ui.p1apa, ui.p1aaa, ui.p1rca, ui.p1aoa], varify);
                    navCf.isGrp = _.some([ui.p1tp, ui.p1ap, ui.p1rc], varify);
                    navCf.isFt = ui.faketracking == '1';
                    navCf.isDmp = ui.dmp === '1'
                }
            }
        }

        return {
            getCf: function() {
                return navCf },
            updateCf: function(){
                set();
            }
        }
    }

})();
