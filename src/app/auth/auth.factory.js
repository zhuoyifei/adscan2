(function() {
    'use strict';

    angular
        .module('adscan')
        .factory('Auth', Auth);


    function Auth($http, $rootScope, url, $q, $state, $window, $cookies) {
        var userInfo = {}

        function login(username, password) {
            var deferred = $q.defer();

            $http.get(url.api2 + '?op=login&username=' + username + '&password=' + password)
                .then(function(result) {
                    if (result.data.status === 200) {
                        userInfo = result.data.data[0]
                        userInfo.token = result.data.token
                        userInfo.ad2token = result.data.ad2token
                        setCookie('userInfo', userInfo)
                        $rootScope.userInfo = userInfo
                        deferred.resolve(userInfo);
                    } else {
                        deferred.reject('用户名或密码错误')
                    }

                }, function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function logout() {
            $cookies.remove('userInfo')
            $rootScope.userInfo = null
            $state.go('login')
        }

        function setCookie(name, value) {
            var exp = new Date(_.now() + 1000 * 60 * 60 * 8);
            $cookies.putObject(name, value, {
                expires: exp
            });
        }
        return {
            login: login,
            logout: logout
        };
    }


})();
