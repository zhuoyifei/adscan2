(function() {
    'use strict';

    angular
        .module('adscan')
        .factory('Resource', Resource);

    /** @ngInject */
    function Resource($http, url, $rootScope) {
        function getData(action, params, isToken, method) {
            var cfg = {};
            cfg.url = url.api;
            if (method) {
                cfg.method = method || 'GET'
            }
            cfg.params = params || {}
            cfg.params.op = action;
            if (isToken) {
                cfg.params.token = $rootScope.userInfo.token
            }

            return $http(cfg).then(function(res) {
                return res.data.data
            }, function(er) {
                return er
            })
        }


        function getKpiData(action, params) {
            var cfg = {};
            cfg.url = url.api;
           
            cfg.params = params || {}
            cfg.params.op = action;
         
            return $http(cfg).then(function(res) {
                return res.data
            }, function(er) {
                return er
            })
        }
        return {
            getData: getData,
            getKpiData:getKpiData
        }

    }
})();
