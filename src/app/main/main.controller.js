(function() {
    'use strict';

    angular
        .module('adscan')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($location, $scope, $anchorScroll, Auth, $cookies, navAuth) {
        var vm = this
        var userInfo = $cookies.getObject('userInfo')
        vm.goto = function(h) {
            $location.hash(h);
            $anchorScroll();
        }
        vm.BIlogin = ''
        vm.getNow = new Date()
        vm.logout = Auth.logout;
        vm.isAdmin=navAuth.getCf().isAdmin
        vm.navCf=navAuth.getCf()
        if (userInfo) {
            vm.userInfo = userInfo;
            vm.uname = userInfo.username;
        }
    }
})();
